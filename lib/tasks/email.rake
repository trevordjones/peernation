namespace :email do
  desc 'send me an email'
  task :send => :environment do
    request_params = {
      first_name: 'Trevor',
      last_name: 'Jones',
      email: 'trevordavidjones@gmail.com'
    }
    MembershipRequestMailer.send_request(request_params).deliver_now
  end
end
