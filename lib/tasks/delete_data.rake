namespace :users do
  desc 'destroy users'
  task :destroy => :environment do
    STDOUT.puts "Are you sure? (yes/no)"
    input = STDIN.gets.strip
    if input == "yes"
      User.all.each do |user|
        UserIndustry.all.each do |ind|
          ind.destroy
        end
        PortfolioDepartment.all.each do |port|
          port.destroy
        end
        UserDepartment.all.each do |dep|
          dep.destroy
        end
        PortfolioAccount.all.each do |port|
          port.destroy
        end
        user.destroy
      end
      Conversation.all.each do |c|
        c.destroy
      end
      ReceivedConversation.all.each do |rc|
        rc.destroy
      end
      SentConversation.all.each do |sc|
        sc.destroy
      end
      Message.all.each do |m|
        m.destroy
      end
      ReceivedMessage.all.each do |rm|
        rm.destroy
      end
    else
      return
    end
  end
end
