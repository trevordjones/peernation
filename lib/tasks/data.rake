namespace :peer do
  namespace :data do
    desc "load test data for developers"
    task :load => :environment do
      require File.join(File.dirname(__FILE__), '../../db/data.rb')
    end
  end
end
