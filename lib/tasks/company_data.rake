require 'csv'
namespace :industry_focus_on do
  desc 'load industries'
  task :load => :environment do
    texts = File.read("#{Rails.root}/public/PN_industries_focus_on.csv")
    csv = CSV.parse(texts, :headers => true)
    csv.each do |row|
      unless row[0].nil?
        name = row[0]
        industry = Industry.find_by(name: name)
        if industry
          industry.update(focus_on: true)
        else
          Industry.create(name: name, focus_on: true)
        end
      end
    end
  end
end

namespace :industry_sell_for do
  desc 'load industries sell for'
  task :load => :environment do
    texts = File.read("#{Rails.root}/public/PN_industries_sell_for.csv")
    csv = CSV.parse(texts, :headers => true)
    csv.each do |row|
      unless row[0].nil?
        name = row[0]
        industry = Industry.find_by(name: name)
        if industry
          industry.update(sell_for: true)
        else
          Industry.create(name: name, sell_for: true)
        end
      end
    end
  end
end

namespace :companies do
  desc 'load departments'
  task :load => :environment do
    texts = File.read("#{Rails.root}/public/PN_companies.csv")
    csv = CSV.parse(texts, :headers => true)
    csv.each do |row|
      company_name = row[0].sub(/\s+\Z/, "")
      city = row[1].sub(" ", "") if row[1][0] == " "
      state = row[2].sub(" ", "") if row[2][0] == " "
      industries = []
      first_industry = row[3].sub(" ", "").sub(/\s+\Z/, "")
      second_industry = row[4].sub(" ", "").sub(/\s+\Z/, "") unless row[4].nil?
      industries << first_industry
      industries << second_industry unless second_industry.nil?
      company = Company.create(name: company_name, state: state, city: city)
      industries.each do |ind|
        industry = Industry.find_by(name: ind)
        if industry.nil?
          industry = Industry.create(name: ind)
        end
        CompanyIndustry.create(company_id: company.id, industry_id: industry.id)
      end
    end
  end
end

namespace :contacts do
  desc 'load contacts'
  task :load => :environment do
    texts = File.read("#{Rails.root}/public/contacts.csv")
    csv = CSV.parse(texts, :headers => true)
    csv.each do |row|
      contact = row[0]
      ContactLevel.create(name: contact)
    end
  end
end

namespace :departments do
  desc 'load contacts'
  task :load => :environment do
    texts = File.read("#{Rails.root}/public/departments.csv")
    csv = CSV.parse(texts, :headers => true)
    csv.each do |row|
      department = row[0].rstrip
      Department.create(name: department)
    end
  end
end

namespace :logos do
  desc 'load logos'
  task :load => :environment do
    folder_path = "#{Rails.root}/public/logos"
    logos = Dir.entries(folder_path)
    logos.reject!{|l| l == "." || l == ".." || l == ".DS_Store"}
    companies = []
    files = []
    counts = []
    logos.each do |logo|
      file = File.open("#{folder_path + "/" + logo}")
      dimensions = FastImage.size(file)
      filename = File.basename(logo, File.extname(logo))
      term = logo.sub(".png", "").gsub("-", " ").downcase.split(" ")
      search_term = term.first
      ["international", "mutual", "alliance", "national", "new", "owens", "public", "state", "western", "williams", "american", "kraft", "l", "pacific", "the", "united", "best", "boston", "general", "global", "applied", "energy"].each do |name|
        if name == term.first
          search_term = term.first + " " + term.second
        end
      end
      
      if search_term == "universal"
        search_term = term.first if term.size == 1
        search_term = term.first + " " + term.second if term.size > 1
      end
      
      company = Company.where("lower(name) LIKE ?", "%#{search_term.downcase}%").first
      if company.nil?
        files << filename
      else
        companies << company
      end
      if !company.nil?
        if !dimensions.nil?
          company.update(logo: file, height: dimensions[1], width: dimensions[0]) if company.logo.url.nil?
        end
      end
    end
    # files.each do |f|
    #   CSV.open("#{Rails.root}/public/rename_logos.csv", "a+") do |csv|
    #     csv << [f]
    #   end
    # end
  end
end
