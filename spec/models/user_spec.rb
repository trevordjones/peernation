require 'rails_helper'

describe 'User' do
  let(:user_params) {
    {
      email: 'aramsey@example.com',
      first_name: "Aaron",
      last_name: 'Ramsey',
      position: 'Sales',
      password: 'password',
      password_confirmation: 'password'
    }
  }
  describe 'generate_username' do
    it "generates and saves a random username" do
      user = User.create(user_params)
      expect(user.anonymous_username).to_not be_nil
    end
  end

end
