# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160514163727) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "app_images", force: :cascade do |t|
    t.string   "name"
    t.string   "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "attachments", force: :cascade do |t|
    t.string   "attachment"
    t.integer  "message_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comments", force: :cascade do |t|
    t.text     "content"
    t.integer  "commentable_id"
    t.integer  "commentable_type"
    t.string   "author"
    t.integer  "user_id"
    t.string   "comment_type"
    t.string   "ancestry"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "title"
    t.string   "email"
    t.string   "phone_number"
    t.string   "visibility"
  end

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.string   "size"
    t.string   "city"
    t.string   "state"
    t.string   "logo"
    t.integer  "industry_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "num_of_users"
    t.integer  "num_of_comments"
    t.integer  "width"
    t.integer  "height"
  end

  create_table "company_departments", force: :cascade do |t|
    t.integer  "company_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "department_id"
  end

  create_table "company_industries", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "industry_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "connections", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "peer_id"
    t.string   "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contact_levels", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "conversations", force: :cascade do |t|
    t.string   "subject"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "departments", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "industries", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "sell_for",   default: false
    t.boolean  "focus_on",   default: false
  end

  create_table "member_requests", force: :cascade do |t|
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "role"
  end

  create_table "messages", force: :cascade do |t|
    t.string   "body"
    t.integer  "conversation_id"
    t.integer  "sender_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "notifications", force: :cascade do |t|
    t.integer  "notified_id"
    t.integer  "notifier_id"
    t.string   "notification_type"
    t.boolean  "read",              default: false
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "record_id"
  end

  create_table "portfolio_accounts", force: :cascade do |t|
    t.string   "open_to"
    t.string   "comments"
    t.integer  "company_id"
    t.integer  "user_id"
    t.integer  "industry_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "portfolio_departments", force: :cascade do |t|
    t.integer  "department_id"
    t.integer  "portfolio_account_id"
    t.integer  "user_id"
    t.integer  "company_id"
    t.integer  "contact_level_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "products", force: :cascade do |t|
    t.string   "product"
    t.string   "pay_by"
    t.decimal  "amount",     precision: 8, scale: 2
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "received_conversations", force: :cascade do |t|
    t.integer  "conversation_id"
    t.integer  "receiver_id"
    t.boolean  "archived",        default: false
    t.boolean  "trashed",         default: false
    t.boolean  "opened",          default: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  create_table "received_messages", force: :cascade do |t|
    t.integer  "message_id"
    t.integer  "receiver_id"
    t.boolean  "archived",    default: false
    t.boolean  "trashed",     default: false
    t.boolean  "opened",      default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "referral_resources", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "token"
    t.string   "link"
    t.integer  "referrals_made"
    t.integer  "referrals_accepted"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "referrals", force: :cascade do |t|
    t.integer  "referred_id"
    t.integer  "referrer_id"
    t.string   "status"
    t.string   "email"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "sent_conversations", force: :cascade do |t|
    t.integer  "conversation_id"
    t.integer  "sender_id"
    t.boolean  "archived",        default: false
    t.boolean  "trashed",         default: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  create_table "target_accounts", force: :cascade do |t|
    t.string   "contact_level"
    t.string   "open_to"
    t.string   "comments"
    t.integer  "company_id"
    t.integer  "user_id"
    t.integer  "department_id"
    t.integer  "industry_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.boolean  "active",        default: false
  end

  create_table "target_departments", force: :cascade do |t|
    t.integer "department_id"
    t.integer "target_account_id"
    t.integer "user_id"
    t.integer "contact_level_id"
  end

  create_table "user_departments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "department_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "user_industries", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "industry_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "target_industry_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                                          default: "",    null: false
    t.string   "encrypted_password",                             default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                                  default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "full_name"
    t.string   "anonymous_username"
    t.string   "city"
    t.string   "state"
    t.string   "phone_number"
    t.string   "position"
    t.string   "summary"
    t.decimal  "referral_fee",           precision: 8, scale: 2
    t.integer  "score"
    t.string   "linkedin_url"
    t.boolean  "terms_of_use",                                   default: false
    t.datetime "suspend_date"
    t.datetime "expiration_date"
    t.integer  "product_id"
    t.integer  "connect_id"
    t.string   "provider"
    t.string   "uid"
    t.boolean  "approved",                                       default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.boolean  "rejected",                                       default: false
    t.string   "currently_sell"
    t.string   "role"
    t.string   "referral_id"
    t.string   "avatar"
    t.string   "linkedin_photo"
    t.boolean  "active",                                         default: true
    t.boolean  "submitted",                                      default: false
    t.boolean  "admin_role"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
