class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string   "subject"
      t.string   "body"
      t.integer  "conversation_id"
      t.integer  "sender_id"
      t.integer  "receiver_id"

      t.timestamps null: false
    end
  end
end
