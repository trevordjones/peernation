class AddBooleanConversationMessage < ActiveRecord::Migration
  def change
    add_column :messages, :opened, :boolean
    add_column :conversations, :opened, :boolean
    remove_column :messages, :subject
  end
end
