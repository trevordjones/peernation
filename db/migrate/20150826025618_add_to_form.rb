class AddToForm < ActiveRecord::Migration
  def change
    remove_column :member_requests, :name, :string
    add_column :member_requests, :first_name, :string
    add_column :member_requests, :last_name, :string
    add_column :member_requests, :role, :string
  end
end
