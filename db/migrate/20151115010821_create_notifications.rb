class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer :notified_id
      t.integer :notifier_id
      t.string :notification_type
      t.boolean :read, default: false

      t.timestamps null: false
    end

    add_column :users, :active, :boolean, default: true
    change_column :users, :terms_of_use, :boolean, default: false
    change_column :users, :rejected, :boolean, default: false
    change_column :conversations, :opened, :boolean, default: false
    change_column :messages, :opened, :boolean, default: false
    rename_column :connections, :connect_id, :peer_id

  end
end
