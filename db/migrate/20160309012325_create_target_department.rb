class CreateTargetDepartment < ActiveRecord::Migration
  def change
    create_table :target_departments do |t|
      t.integer :department_id
      t.integer :target_account_id
      t.integer :user_id
      t.integer :contact_level_id
    end
  end
  
  add_column :target_accounts, :active, :boolean, default: false
end
