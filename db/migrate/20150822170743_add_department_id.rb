class AddDepartmentId < ActiveRecord::Migration
  def change
    add_column :company_departments, :department_id, :integer
    remove_column :company_departments, :industry_id
  end
end
