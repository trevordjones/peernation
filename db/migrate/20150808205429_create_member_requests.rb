class CreateMemberRequests < ActiveRecord::Migration
  def change
    create_table :member_requests do |t|
      t.string "name"
      t.string "email"

      t.timestamps null: false
    end
  end
end
