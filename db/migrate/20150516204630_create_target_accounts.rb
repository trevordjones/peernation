class CreateTargetAccounts < ActiveRecord::Migration
  def change
    create_table :target_accounts do |t|
      t.string   "contact_level"
      t.string   "open_to"
      t.string   "comments"
      t.integer  "company_id"
      t.integer  "user_id"
      t.integer  "department_id"
      t.integer  "industry_id"
      t.datetime "created_at",        null: false
      t.datetime "updated_at",        null: false

      t.timestamps null: false
    end
  end
end
