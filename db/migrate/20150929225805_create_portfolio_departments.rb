class CreatePortfolioDepartments < ActiveRecord::Migration
  def change
    create_table :portfolio_departments do |t|
      t.integer :department_id
      t.integer :portfolio_account_id
      t.integer :user_id
      t.integer :company_id
      t.integer :contact_level_id

      t.timestamps null: false
    end

    create_table :contact_levels do |t|
      t.string :name

      t.timestamps null: false
    end

    add_column :users, :linkedin_photo, :string
    add_column :user_industries, :target_industry_id, :integer
    rename_column :users, :linked_in, :linkedin_url
    remove_column :portfolio_accounts, :department_id, :integer
    remove_column :portfolio_accounts, :contact_level, :integer
  end
end
