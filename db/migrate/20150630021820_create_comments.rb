class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text :content
      t.integer :commentable_id
      t.integer :commentable_type
      t.string :author
      t.integer :user_id
      t.string :comment_type
      t.string :ancestry

      t.timestamps null: false

      # add_index :comments, :ancestry
    end
  end
end
