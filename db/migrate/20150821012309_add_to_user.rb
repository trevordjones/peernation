class AddToUser < ActiveRecord::Migration
  def change
    add_column :users, :department_id, :integer
    add_column :users, :industry_id, :integer
    add_column :users, :currently_sell, :string
  end
end
