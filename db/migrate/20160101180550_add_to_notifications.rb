class AddToNotifications < ActiveRecord::Migration
  def change
    create_table :received_messages do |t|
      t.integer :message_id
      t.integer :receiver_id
      t.boolean :archived, default: false
      t.boolean :trashed, default: false
      t.boolean :opened, default: false
      t.timestamps null: false
    end
    
    create_table :received_conversations do |t|
      t.integer :conversation_id
      t.integer :receiver_id
      t.boolean :archived, default: false
      t.boolean :trashed, default: false
      t.boolean :opened, default: false
      t.timestamps null: false
    end
    
    create_table :sent_conversations do |t|
      t.integer :conversation_id
      t.integer :sender_id
      t.boolean :archived, default: false
      t.boolean :trashed, default: false
      t.timestamps null: false
    end
    
    create_table :attachments do |t|
      t.string :attachment
      t.integer :message_id
      t.timestamps null: false
    end
    
    add_column :notifications, :record_id, :integer
    add_column :users, :submitted, :boolean, default: false
    remove_column :conversations, :archive, :boolean
    remove_column :conversations, :trash, :boolean
    remove_column :conversations, :opened, :boolean
    remove_column :conversations, :body, :string
    remove_column :conversations, :receiver_id, :integer
    remove_column :conversations, :sender_id, :integer
    remove_column :messages, :receiver_id, :integer
    remove_column :messages, :opened, :boolean
    remove_column :messages, :archive, :boolean
    remove_column :messages, :trash, :boolean
  end
end
