class AddToIndustry < ActiveRecord::Migration
  def change
    add_column :industries, :sell_for, :boolean, default: false
    add_column :industries, :focus_on, :boolean, default: false
  end
end
