class AddNumbersToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :num_of_users, :integer
    add_column :companies, :num_of_comments, :integer
  end
end
