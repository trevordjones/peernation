class AddToComments < ActiveRecord::Migration
  def change
    add_column :comments, :first_name, :string
    add_column :comments, :last_name, :string
    add_column :comments, :title, :string
    add_column :comments, :email, :string
    add_column :comments, :phone_number, :string
    add_column :comments, :visibility, :string

    add_column :companies, :width, :integer
    add_column :companies, :height, :integer

    add_column :conversations, :archive, :boolean, default: false
    add_column :conversations, :trash, :boolean, default: false

    add_column :messages, :archive, :boolean, default: false
    add_column :messages, :trash, :boolean, default: false
  end
end
