class AddDefaultValues < ActiveRecord::Migration
  def change
    change_column_default(:users, :approved, false)
    change_column_default(:users, :admin, false)
  end
end
