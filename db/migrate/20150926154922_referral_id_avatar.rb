class ReferralIdAvatar < ActiveRecord::Migration
  def change
    add_column :users, :referral_id, :string
    add_column :users, :avatar, :string
  end
end
