class FullNameAdminChange < ActiveRecord::Migration
  def change
    rename_column :users, :first_name, :full_name
    remove_column :users, :last_name, :string
    add_column :users, :admin_role, :boolean
  end
end
