class CreateCompanyDepartments < ActiveRecord::Migration
  def change
    create_table :company_departments do |t|
      t.integer  "company_id"
      t.integer  "industry_id"

      t.timestamps null: false
    end
  end
end
