class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string   "product"
      t.string   "pay_by"
      t.decimal  "amount",     precision: 8, scale: 2

      t.timestamps null: false
    end
  end
end
