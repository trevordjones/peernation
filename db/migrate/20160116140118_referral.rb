class Referral < ActiveRecord::Migration
  def change
    create_table :referral_resources do |t|
      t.integer :user_id
      t.string :token
      t.string :link
      t.integer :referrals_made
      t.integer :referrals_accepted
      
      t.timestamps null: false
    end
    
    create_table :referrals do |t|
      t.integer :referred_id
      t.integer :referrer_id
      t.string :status
      t.string :email
      
      t.timestamps null: false
    end
  end
end
