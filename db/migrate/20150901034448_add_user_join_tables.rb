class AddUserJoinTables < ActiveRecord::Migration
  def change
    create_table :user_departments do |t|
      t.integer "user_id"
      t.integer "department_id"

      t.timestamps null: false
    end
    create_table :user_industries do |t|
      t.integer "user_id"
      t.integer "industry_id"

      t.timestamps null: false
    end

    remove_column :users, :department_id, :integer
    remove_column :users, :industry_id, :integer
    add_column :users, :role, :string
  end
end
