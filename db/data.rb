jim = User.new(
  email: 'jhalpert@dundermifflin.com',
  full_name: 'Jim Halpert',
  position: 'Sales',
  role: 'Sales Professional',
  currently_sell: 'paper',
  password: 'password1',
  password_confirmation: 'password1',
  city: 'Scranton',
  state: 'PA',
  anonymous_username: 'tallngoofy',
  linkedin_url: 'https://linkedin.com',
  linkedin_photo: 'http://vignette1.wikia.nocookie.net/theoffice/images/9/9a/Jim.jpg/revision/latest?cb=20131011080102',
  terms_of_use: true,
  submitted: true)
michael = User.new(email: 'mscott@dundermifflin.com',
  full_name: 'Michael Scott',
  position: 'Manager',
  currently_sell: 'paper',
  role: 'Executive',
  password: 'password2',
  password_confirmation: 'password2',
  city: 'Scranton',
  state: 'PA',
  anonymous_username: 'sugarbaby',
  linkedin_url: 'https://linkedin.com',
  linkedin_photo: 'http://static.businessinsider.com/image/51cb012fecad04ff3a000005/image.jpg',
  terms_of_use: true,
  submitted: true)
dwight = User.new(email: 'dschrute@dundermifflin.com',
  full_name: 'Dwight Schrute',
  position: 'Assistant To The Regional Manager',
  role: 'Sales Professional',
  currently_sell: 'paper',
  password: 'schruteface',
  password_confirmation: 'schruteface',
  city: 'Scranton',
  state: 'PA',
  anonymous_username: 'beetsrule',
  linkedin_url: 'https://linkedin.com',
  linkedin_photo: 'http://images4.fanpop.com/image/photos/16100000/Dwight-dwight-schrute-16100666-512-288.jpg',
  terms_of_use: true,
  submitted: true)
pam = User.new(email: 'pbeasley@dundermiflin.com',
  full_name: 'Pamela Beasley',
  position: 'Receptionist',
  role: 'Sales Professional',
  currently_sell: 'paper',
  password: 'rtothecep',
  password_confirmation: 'rtothecep',
  city: 'Scranton',
  state: 'PA',
  anonymous_username: 'pbeaser',
  linkedin_url: 'https://linkedin.com',
  linkedin_photo: 'http://www.eonline.com/eol_images/Entire_Site/20080825/425.fischer.theoffice.082508.jpg',
  terms_of_use: true,
  submitted: true)
andy = User.new(email: 'abernard@dundermifflin.com',
  full_name: 'Andy Bernard',
  position: 'Sales',
  role: 'Sales Professional',
  currently_sell: 'Accounting Software',
  password: 'nardpassword',
  password_confirmation: 'nardpassword',
  city: 'Scranton',
  state: 'PA',
  anonymous_username: 'nard-dawg',
  linkedin_url: 'https://linkedin.com',
  linkedin_photo: 'https://hilarywgraham.files.wordpress.com/2012/03/andy-andy-bernard-22624931-380-370.jpg',
  terms_of_use: true,
  submitted: true)
kevin = User.new(email: 'kmalone@dundermifflin.com',
  full_name: 'Kevin Malone',
  position: 'Accountant',
  role: 'Consultant',
  currently_sell: 'Accounting Software',
  password: 'chocolate',
  password_confirmation: 'chocolate',
  city: 'Scranton',
  state: 'PA',
  anonymous_username: 'fattie',
  linkedin_url: 'https://linkedin.com',
  linkedin_photo: 'http://images1.fanpop.com/images/photos/1300000/Kevin-in-Goodbye-Toby-kevin-malone-1392337-1280-720.jpg',
  terms_of_use: true,
  submitted: true)
karen = User.new(email: 'kfilippelli@dundermifflin.com',
  full_name: 'Karen Filippelli',
  position: 'Sales' ,
  role: 'Sales Professional',
  currently_sell: 'Paper',
  password: 'i<3jhalpert',
  password_confirmation: 'i<3jhalpert',
  city: 'Scranton',
  state: 'PA',
  anonymous_username: 'filipi',
  linkedin_url: 'https://linkedin.com',
  linkedin_photo: 'http://images.fanpop.com/images/image_uploads/Karen-the-office--28us-29-41049_624_352.jpg',
  terms_of_use: true,
  submitted: true)
angela = User.new(email: 'amartin@dundermifflin.com',
  full_name: 'Angela Martin',
  position: 'Accountant',
  role: 'Consultant',
  currently_sell: 'Accounting Software',
  password: 'strict4life',
  password_confirmation: 'strict4life',
  city: 'Scranton',
  state: 'PA',
  anonymous_username: 'catlover',
  linkedin_url: 'https://linkedin.com',
  linkedin_photo: 'http://vignette1.wikia.nocookie.net/theoffice/images/c/cc/Angela.PNG/revision/latest?cb=20090316235840',
  terms_of_use: true,
  submitted: true)
holly = User.new(email: 'hflax@dundermifflin.com',
  full_name: 'Holly Flax',
  position: 'HR Rep',
  role: 'Consultant',
  currently_sell: 'Human Resources',
  password: 'thehumanity',
  password_confirmation: 'thehumanity',
  city: 'Scranton',
  state: 'PA',
  anonymous_username: 'hflax',
  linkedin_url: 'https://linkedin.com',
  linkedin_photo: 'http://i.imgur.com/9BJgY.jpg',
  terms_of_use: true,
  submitted: true)
kelly = User.new(email: 'kkapoor@dundermifflin.com',
  full_name: 'Kelly Kapoor',
  position: 'Customer Success',
  role: 'Consultant',
  currently_sell: 'Woof',
  password: 'hot4life',
  password_confirmation: 'hot4life',
  city: 'Scranton',
  state: 'PA',
  anonymous_username: 'k-girl',
  linkedin_url: 'https://linkedin.com',
  linkedin_photo: 'https://theofficescranton.files.wordpress.com/2012/09/kelly-kapoor1.jpg',
  terms_of_use: true,
  submitted: true)

[jim, michael, dwight, pam, andy, kevin, karen, angela, holly, kelly].each do |user|
  user.skip_confirmation!
  if user.save
    puts "#{user.full_name} was saved"
  else
    puts "#{user.full_name} was not saved becuase of #{user.errors.full_messages}"
  end

end

[23,26,467].each do |id|
  company = Company.find(id)
  account = PortfolioAccount.create(company_id: id, user_id: michael.id)
  company.num_of_users = 1
  company.save
  UserDepartment.create(department_id: rand(1..61), user_id: michael.id)
  UserIndustry.create(industry_id: rand(1..100), user_id: michael.id)
  UserIndustry.create(target_industry_id: rand(1..100), user_id: michael.id)
  PortfolioDepartment.create(department_id: rand(1..61), user_id: michael.id, portfolio_account_id: account.id, contact_level_id: rand(1..9) )
end
[54, 567, 247].each do |id|
  company = Company.find(id)
  account = PortfolioAccount.create(company_id: id, user_id: jim.id)
  company.num_of_users = 1
  company.save
  UserDepartment.create(department_id: rand(1..61), user_id: jim.id)
  UserIndustry.create(industry_id: rand(1..100), user_id: jim.id)
  UserIndustry.create(target_industry_id: rand(1..100), user_id: jim.id)
  PortfolioDepartment.create(department_id: rand(1..61), user_id: jim.id, portfolio_account_id: account.id, contact_level_id: rand(1..9) )
end
[78,35,357].each do |id|
  company = Company.find(id)
  account = PortfolioAccount.create(company_id: id , user_id: dwight.id)
  company.num_of_users = 1
  company.save
  UserDepartment.create(department_id: rand(1..61), user_id: dwight.id)
  UserIndustry.create(industry_id: rand(1..100), user_id: dwight.id)
  UserIndustry.create(target_industry_id: rand(1..100), user_id: dwight.id)
  PortfolioDepartment.create(department_id: rand(1..61), user_id: dwight.id, portfolio_account_id: account.id, contact_level_id: rand(1..9) )
end
[49, 385, 394].each do |id|
  company = Company.find(id)
  account = PortfolioAccount.create(company_id: id , user_id: kevin.id)
  company.num_of_users = 1
  company.save
  UserDepartment.create(department_id: rand(1..61), user_id: kevin.id)
  UserIndustry.create(industry_id: rand(1..100), user_id: kevin.id)
  UserIndustry.create(target_industry_id: rand(1..100), user_id: kevin.id)
  PortfolioDepartment.create(department_id: rand(1..61), user_id: kevin.id, portfolio_account_id: account.id, contact_level_id: rand(1..9) )
end
[34, 78, 385].each do |id|
  company = Company.find(id)
  account = PortfolioAccount.create(company_id: id , user_id: karen.id)
  company.num_of_users = 1
  company.save
  UserDepartment.create(department_id: rand(1..61), user_id: karen.id)
  UserIndustry.create(industry_id: rand(1..100), user_id: karen.id)
  UserIndustry.create(target_industry_id: rand(1..100), user_id: karen.id)
  PortfolioDepartment.create(department_id: rand(1..61), user_id: karen.id, portfolio_account_id: account.id, contact_level_id: rand(1..9) )
end
[59, 249, 206].each do |id|
  company = Company.find(id)
  account = PortfolioAccount.create(company_id: id , user_id: angela.id)
  company.num_of_users = 1
  company.save
  UserDepartment.create(department_id: rand(1..61), user_id: angela.id)
  UserIndustry.create(industry_id: rand(1..100), user_id: angela.id)
  UserIndustry.create(target_industry_id: rand(1..100), user_id: angela.id)
  PortfolioDepartment.create(department_id: rand(1..61), user_id: angela.id, portfolio_account_id: account.id, contact_level_id: rand(1..9) )
end
[24, 67, 190].each do |id|
  company = Company.find(id)
  account = PortfolioAccount.create(company_id: id , user_id: kelly.id)
  company.num_of_users = 1
  company.save
  UserDepartment.create(department_id: rand(1..61), user_id: kelly.id)
  UserIndustry.create(industry_id: rand(1..100), user_id: kelly.id)
  UserIndustry.create(target_industry_id: rand(1..100), user_id: kelly.id)
  PortfolioDepartment.create(department_id: rand(1..61), user_id: kelly.id, portfolio_account_id: account.id, contact_level_id: rand(1..9) )
end
[678, 457, 146].each do |id|
  company = Company.find(id)
  account = PortfolioAccount.create(company_id: id , user_id: pam.id)
  company.num_of_users = 1
  company.save
  UserDepartment.create(department_id: rand(1..61), user_id: pam.id)
  UserIndustry.create(industry_id: rand(1..100), user_id: pam.id)
  UserIndustry.create(target_industry_id: rand(1..100), user_id: pam.id)
  PortfolioDepartment.create(department_id: rand(1..61), user_id: pam.id, portfolio_account_id: account.id, contact_level_id: rand(1..9) )
end
[358, 342, 502].each do |id|
  company = Company.find(id)
  account = PortfolioAccount.create(company_id: id , user_id: holly.id)
  company.num_of_users = 1
  company.save
  UserDepartment.create(department_id: rand(1..61), user_id: holly.id)
  UserIndustry.create(industry_id: rand(1..100), user_id: holly.id)
  UserIndustry.create(target_industry_id: rand(1..100), user_id: holly.id)
  PortfolioDepartment.create(department_id: rand(1..61), user_id: holly.id, portfolio_account_id: account.id, contact_level_id: rand(1..9) )
end
[472, 624, 249].each do |id|
  company = Company.find(id)
  account = PortfolioAccount.create(company_id: id , user_id: andy.id)
  company.num_of_users = 1
  company.save
  UserDepartment.create(department_id: rand(1..61), user_id: andy.id)
  UserIndustry.create(industry_id: rand(1..100), user_id: andy.id)
  UserIndustry.create(target_industry_id: rand(1..100), user_id: andy.id)
  PortfolioDepartment.create(department_id: rand(1..61), user_id: andy.id, portfolio_account_id: account.id, contact_level_id: rand(1..9) )
end
