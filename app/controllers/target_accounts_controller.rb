class TargetAccountsController < ApplicationController
  before_filter :authenticate_user!, :set_states

  def new
    @target = TargetAccount.new
  end

  def create
    if params_nil? && (params[:next] || params[:profile])
      render json: {next: true}.to_json
    else
      company = FindCompany.new(company_params).create_portfolio
      if company.errors.empty?
        target = TargetAccount.new(user_id: current_user.id, company_id: company.id)
        if CreateTargetAssociations.new(target, current_user, company_params).create_target_associations
          if params[:next]
            flash[:notice] = "#{current_user.target_accounts.size} Companies added to your target accounts"
            render json: {next: true}.to_json
          else
            render json: "#{current_user.target_accounts.size} Companies added to your target accounts".to_json
          end
        else
          render json: target.errors.full_messages.to_json, status: :unprocessable_entity
        end
      else
        render json: company.errors.full_messages.to_json, status: :unprocessable_entity
      end
    end
  end
  
  private
  
  def company_params
    {
      id: params[:company_id],
      name: params[:company],
      state: params[:state],
      city: params[:city],
      department_ids: params[:department_ids],
      contact_level_ids: params[:contact_level_ids],
      industry_ids: params[:industry_ids],
      new_company: params[:new_company]
    }
  end
  
  def params_nil?
    return false if params[:quick_add]
    params[:company].nil? && params[:industry_ids].nil? && params[:city].nil? && params[:state].nil? && params[:department_ids].nil? && params[:contact_level_ids].nil?
  end
  
  def target_params
    params.require(:target_account).permit(:id)
  end

end
