class MembersController < ApplicationController
  before_filter :approved_user

  def index
    # @members = User.where(approved: true).order(created_at: :desc)
    # @members = @members.by_name(params["name"]) if params["name"].present?
  end

  def show
    @member = User.find(params[:id])
    read_notification(params[:note_id]) if params[:read]
  end

  def update
  end

  def create_connection
    @member = User.find(params[:member_id])
    @members = User.order(created_at: :desc)
    render :index => @members
  end
end
