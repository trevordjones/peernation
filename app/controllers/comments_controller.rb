class CommentsController < ApplicationController

  def create
    @comment = build_commentable
    @comment.author = current_user.anonymous_username
    @comment.user_id = current_user.id
    validate_contact(@comment) if comment_params[:comment_type] == 'Contact'
    
    if @comment.errors.empty?
      if @comment.save
        if params[:company_id]
          company = Company.find(params[:company_id])
          company.num_of_comments ? company.num_of_comments += 1 : company.num_of_comments = 1
          company.save
        end
        render json: @comment
      else
        render json: @comment.errors.full_messages, status: :unprocessable_entity
      end
    else
      render json: @comment.errors.full_messages, status: :unprocessable_entity
    end
    
  end

  def validate_contact(comment)
    comment.errors.add(:first_name, "can't be blank") unless params[:first_name]
    comment.errors.add(:last_name, "can't be blank") unless params[:last_name]
    comment.errors.add(:title, "can't be blank") unless params[:title]
    comment.errors.add(:content, "can't be blank") unless params[:content]
  end
  
  def build_commentable
    if comment_commentable?
      comment = Comment.new(comment_params)
      comment.parent_id = params[:parent_id]
    else
      commentable = Company.find(params[:company_id])
      comment = commentable.comments.build(comment_params)
    end
    comment
  end

  def comment_commentable?
    params[:parent_id]
  end

  def comment_params
    params.require(:comment).permit(:content,
                                    :parent_id,
                                    :comment_id,
                                    :author,:user_id,
                                    :comment_type,
                                    :commentable_id,
                                    :commentable_type,
                                    :ancestry,
                                    :first_name,
                                    :last_name,
                                    :title,
                                    :email,
                                    :phone_number,
                                    :visibility)
  end

end
