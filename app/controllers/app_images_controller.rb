class AppImagesController < ApplicationController
  before_filter :authorize_admin

  def new
    @image = AppImage.new
  end

  def create
    @image = AppImage.new(image_params)
    if @image.save
      redirect_to new_app_image_path
    else
      render :new
    end
  end

  def image_params
    params.require(:app_image).permit(:image, :name)
  end
end
