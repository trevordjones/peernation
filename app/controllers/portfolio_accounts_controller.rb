class PortfolioAccountsController < ApplicationController
  before_filter :redirect_if_admin
  def new
    @portfolio = PortfolioAccount.new
    @states = YAML.load(File.read(File.expand_path("#{Rails.root}/config/states.yml", __FILE__)))
  end

  def create
    if params_nil? && (params[:next] || params[:profile])
      route_to = {next: true} if params[:next]
      route_to = {profile: true} if params[:profile]
      render json: route_to.to_json
    else
      company = FindCompany.new(company_params).create_portfolio
      if company.errors.empty?
        portfolio = PortfolioAccount.new(user_id: current_user.id, company_id: company.id)
        if portfolio.save
          build_associations(portfolio)
          company.num_of_users ? company.num_of_users += 1 : company.num_of_users = 1
          company.save
          if params[:next]
            flash[:notice] = "#{current_user.portfolio_companies.size} Companies added to your account"
            render json: {next: true}.to_json
          elsif params[:profile]
            flash[:notice] = "#{current_user.portfolio_companies.size} Companies added to your account"
            render json: {profile: true}.to_json
          else
            render json: "#{current_user.portfolio_companies.size} Companies added to your account".to_json
          end
        else
          render json: portfolio.errors.full_messages.to_json, status: :unprocessable_entity
        end
      else
        render json: company.errors.full_messages.to_json, status: :unprocessable_entity
      end
    end
  end

  def edit
    @portfolio = PortfolioAccount.find(params[:id])
  end

  def update
    department_ids = params[:portfolio_account][:departments][:department_ids].reject{|id| id.empty?}.map{|id| id.to_i}
    contact_level_ids = params[:portfolio_account][:contact_levels][:contact_level_ids].reject{|id| id.empty?}.map{|id| id.to_i}
    @portfolio = PortfolioAccount.find(params[:id])
    if department_ids.empty?
      flash.now[:danger] = "Please select at least one department"
      render :edit
    elsif contact_level_ids.empty?
      flash.now[:danger] = "Please select at least one contact"
      render :edit
    else
      update_associations(department_ids, contact_level_ids, @portfolio)
      flash[:success] = "Profile updated"
        redirect_to profile_path
    end
  end

  def destroy
    portfolio = PortfolioAccount.find(params[:id])
    company = portfolio.company
    company.update(num_of_users: company.num_of_users - 1)
    if portfolio.destroy
      flash[:success] = "Portfolio destroyed"
    else
      flash[:warning] = "Something went wrong, please try again"
    end
    redirect_to profile_path
  end

  private

  def company_params
    {
      id: params[:company_id],
      name: params[:company],
      state: params[:state],
      city: params[:city],
      department_ids: params[:department_ids],
      contact_level_ids: params[:contact_level_ids],
      industry_ids: params[:industry_ids],
      new_company: params[:new_company]
    }
  end

  def params_nil?
    (params[:company].nil? || params[:company].empty?) && params[:department_ids].nil? && params[:industry_ids].nil? && params[:city].nil? && params[:state].nil? && params[:contact_level_ids].nil?
  end

  def build_associations(portfolio)
    build_departments(portfolio)
    build_contact_levels(portfolio)
  end

  def update_associations(department_ids, contact_level_ids, portfolio)
    update_departments(department_ids, portfolio)
    update_contact_levels(contact_level_ids, portfolio)
  end

  def build_departments(portfolio)
    params[:department_ids].each do |id|
      PortfolioDepartment.create(user_id: current_user.id, department_id: id, portfolio_account_id: portfolio.id)
    end
  end

  def build_contact_levels(portfolio)
    params[:contact_level_ids].each do |id|
      PortfolioDepartment.create(user_id: current_user.id, contact_level_id: id, portfolio_account_id: portfolio.id)
    end
  end

  def update_departments(ids, portfolio)
    portfolio_ids = portfolio.departments.map(&:id)
    destroy_ids = portfolio_ids - ids
    destroy_ids.each do |id|
      PortfolioDepartment.find_by(user_id: current_user.id, department_id: id, portfolio_account_id: portfolio.id).destroy
    end
    create_ids = ids - portfolio_ids
    create_ids.each do |id|
      PortfolioDepartment.create(user_id: current_user.id, department_id: id, portfolio_account_id: portfolio.id)
    end
  end

  def update_contact_levels(ids, portfolio)
    portfolio_ids = portfolio.contact_levels.map(&:id)
    destroy_ids = portfolio_ids - ids
    destroy_ids.each do |id|
      PortfolioDepartment.find_by(user_id: current_user.id, contact_level_id: id, portfolio_account_id: portfolio.id).destroy
    end
    create_ids = ids - portfolio_ids
    create_ids.each do |id|
      PortfolioDepartment.create(user_id: current_user.id, contact_level_id: id, portfolio_account_id: portfolio.id)
    end
  end

  def portfolio_params
    params.require(:portfolio_account).permit(:company_id, department_ids: [], contact_level_ids: [])
  end

end
