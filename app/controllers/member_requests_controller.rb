class MemberRequestsController < ApplicationController

  def index
  end

  def create
    begin
      error = ["Please fill in the form"] if params[:member_request].nil?
      request = MemberRequest.new(request_params)
      if request.save
        @message = "Thank you! As soon as we are up and running we will send you a email invitation to join Peer Nation."
        render json: @message.to_json
      else
        render json: request.errors.full_messages, status: :unprocessable_entity
      end
    rescue
      render json: error, status: :unprocessable_entity
    end
    MembershipRequestMailer.send_request(request_params).deliver_now unless @message.nil?
    MembershipRequestMailer.send_to_peernation(request_params).deliver_now unless @message.nil?
  end

  private
  def request_params
    params.require(:member_request).permit(:email, :first_name, :last_name, :role)
  end

end
