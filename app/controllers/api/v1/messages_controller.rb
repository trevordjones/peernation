module Api
  module V1
    class MessagesController < ApplicationController
      
      def show
        @conversation = Conversation.find(params[:id])
      end
      
    end
  end
end
