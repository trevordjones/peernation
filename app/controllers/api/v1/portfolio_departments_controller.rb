module Api
  module V1
    class PortfolioDepartmentsController < ApplicationController
      def index
        #Use this query when we have enough members to fill the department search
        # @departments = Department.joins(:portfolio_users).distinct
        @departments = Department.all
      end
    end
  end
end
