module Api
  module V1
    class CompaniesController < ApplicationController
      skip_before_filter :authenticate_user_or_admin
      def index
        @companies = Company.order('num_of_users desc nulls last')
        @companies = @companies.by_name(params["name"]) if params[:name]
      end

      def search
        industry_ids = params[:industry_ids].gsub("[", "").gsub("]", "").split(",")
        cities = params[:cities].gsub("[", "").gsub("]", "").split(",")
        companies = Company.order(num_of_users: :asc)
        companies = companies.by_industry(industry_ids) if !industry_ids.empty?
        companies = companies.by_city(cities) if !cities.empty?
        companies = companies.distinct

        render json: companies.to_json
      end

      def async_search
        companies = Company.where("lower(name) LIKE ?", "%#{params[:companies].downcase}%").limit(10)
        render json: companies.to_json
      end

      def city_search
        cities = Company.where("lower(city) LIKE?", "%#{params[:city].downcase}%").limit(10).map(&:city).uniq
        render json: cities.to_json
      end

    end
  end
end
