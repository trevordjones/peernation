module Api
  module V1
    class NotificationsController < ApplicationController
      
      def index
        @notifications = Notification.unread_notifications(current_user).order(created_at: :desc)
      end
      
      def destroy
        notification = Notification.find(params[:id])
        if notification.destroy
          render json: notification.to_json
        else
          render json: "Not deleted - please try again.".to_json, status: :unprocessable_entity
        end
      end
      
    end
  end
end
