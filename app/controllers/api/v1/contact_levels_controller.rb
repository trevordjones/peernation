module Api
  module V1
    class ContactLevelsController < ApplicationController
      def index
        render json: ContactLevel.all
      end
    end
  end
end
