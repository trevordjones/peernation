module Api
  module V1
    class ConnectionsController < ApplicationController
      def index
        @connections = current_user.peers
      end

      def recipients
        keyword = params[:recipient].downcase
        @recipients = current_user.peers.where("lower(first_name) LIKE ? OR lower(last_name) LIKE ? OR lower(email) LIKE ?", "%#{keyword}%", "%#{keyword}%", "%#{keyword}%")
      end

      def recipient
        @recipient = User.find(params[:id])
      end
      
      def create
        member = User.find(params[:member_id])
        subject = "#{current_user.full_name} has requested you as a peer!"
        if RequestConnection.new(current_user, member, subject, params[:body]).send_request
          render json: 'Your request was sent!'.to_json
        else
          render json: "Something happened. Please try your request again.".to_json, status: :unprocessable_entity
        end
      end
    end
  end
end