module Api
  module V1
    class CommentsController < ApplicationController
      before_filter :authenticate_user!

      def index
        company = Company.find(params[:id])
        comments = company.comments
        comments = Comment.by_contact if !params[:contact] == "true"
        comments = Comment.by_question if !params[:question] == "true"
        comments = Comment.by_comment if !params[:comment] == "true"
        render json: comments
      end

    end
  end
end
