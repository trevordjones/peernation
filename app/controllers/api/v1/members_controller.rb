module Api
  module V1
    class MembersController < ApplicationController

      def search
        cities = params[:cities].gsub("[", "").gsub("]", "").split(",")
        department_ids = params[:department_ids].gsub("[", "").gsub("]", "").split(",")
        contacts = params[:contacts].gsub("[", "").gsub("]", "").split(",")
        industries = params[:industries].gsub("[", "").gsub("]", "").split(",")
        users = User.joins(:portfolio_accounts).distinct.limit(50)
        users = users.by_department(department_ids) if !department_ids.empty?
        users = users.by_city(cities) if !cities.empty?
        users = users.by_contact(contacts) if !contacts.empty?
        users = users.by_industry(industries) if !industries.empty?
        @members = users.where(approved: true).where.not(id: current_user.id).distinct
      end

      def company_members_search
        cities = params[:cities].gsub("[", "").gsub("]", "").split(",")
        department_ids = params[:department_ids].gsub("[", "").gsub("]", "").split(",")
        contacts = params[:contacts].gsub("[", "").gsub("]", "").split(",")
        industries = params[:industries].gsub("[", "").gsub("]", "").split(",")
        company = Company.find(params[:id])
        users = company.portfolio_users
        users = users.by_department(department_ids) if !department_ids.empty?
        users = users.by_city(cities) if !cities.empty?
        users = users.by_contact(contacts) if !contacts.empty?
        users = users.by_industry(industries) if !industries.empty?
        @members = users.distinct
      end

      def build_response(users)
        members = []
        users.each do |user|
          json_user = user.serializable_hash
          json_user[:portfolio] = []
          index = 0
          user.portfolio_accounts.each do |portfolio|
            company = portfolio.company
            json_user[:portfolio] << company.serializable_hash
            json_user[:portfolio][index][:industries] = company.comp_industries
            json_user[:portfolio][index][:departments] = portfolio.departments
            json_user[:portfolio][index][:contacts] = portfolio.contact_levels
            index += 1
          end
          members << json_user
        end
        members
      end

      def city_search
        cities = User.where("lower(city) LIKE?", "%#{params[:city]}%").limit(3).map(&:city).uniq
        render json: cities.to_json
      end

      def company_members
        company = Company.find(params[:id])
        @members = company.portfolio_users
      end

      def peers_search
        @members = current_user.peers
      end

      def company_peers_search
        company = Company.find(params[:id])
        @members = company.portfolio_users.merge(current_user.peers)
      end

    end
  end
end
