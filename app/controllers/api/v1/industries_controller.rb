module Api
  module V1
    class IndustriesController < ApplicationController
      skip_before_filter :authenticate_user_or_admin
      def index
        render json: Industry.focus_on.order(name: :asc)
      end

      def search_by_id
        ids = params[:id].gsub("[", "").gsub("]", "").split(",")
        @industries = Industry.by_id(ids)
      end

      def show
        @industry = Industry.find(params[:id])
      end
    end
  end
end
