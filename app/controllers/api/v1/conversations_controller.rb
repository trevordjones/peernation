require 'will_paginate/array'
module Api
  module V1
    class ConversationsController < ApplicationController
      def index
        @sent_conversations = current_user.sent_conversations.paginate(per_page: 5, page: params[:page])
        @received_conversations = current_user.received_conversations.paginate(per_page: 5, page: params[:page])
        @archived_conversations = current_user.archived_conversations.paginate(per_page: 5, page: params[:page])
        @trashed_conversations = current_user.trashed_conversations.paginate(per_page: 5, page: params[:page])
      end

      def sent
        @sent = current_user.conversations_sent.paginate(per_page: 5, page: params[:page])
      end

      def received
        @received = current_user.active_conversations_received.paginate(per_page: 5, page: params[:page])
      end

      def archived
        @archived = current_user.archived_conversations.uniq.paginate(per_page: 5, page: params[:page])
      end

      def trashed
        @trashed = current_user.trashed_conversations.uniq.paginate(per_page: 5, page: params[:page])
      end

      def archive
        conversation_ids = params[:conversation_ids].gsub("[", "").gsub("]", "").split(",")
        begin
          archive_conversations(conversation_ids)
          flash[:success] = "Message(s) archived"
          render json: "wahoo!".to_json
        rescue
          render json: "Oops! Try archiving again".to_json, status: :unprocessable_entity
        end
      end
      
      def archive_conversations(conversation_ids)
        ReceivedConversation.where(conversation_id: conversation_ids).update_all(archived: true, trashed: false) &&
        SentConversation.where(conversation_id: conversation_ids).update_all(archived: true, trashed: false)
      end

      def trash
        conversation_ids = params[:conversation_ids].gsub("[", "").gsub("]", "").split(",")
        begin
          trash_conversations(conversation_ids)
          flash[:success] = "Message(s) trashed"
          render json: "".to_json
        rescue
          render json: "Oops! Try trashing again".to_json, status: :unprocessable_entity
        end
      end
      
      def trash_conversations(conversation_ids)
        ReceivedConversation.where(conversation_id: conversation_ids).update_all(archived: false, trashed: true) &&
        SentConversation.where(conversation_id: conversation_ids).update_all(archived: false, trashed: true)
      end
    end
  end
end
