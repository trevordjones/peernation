module Api
  module V1
    class UsersController < ApplicationController

      def index
        if current_user
          @members = User.joins(:portfolio_accounts).where(approved: true).where.not(id: current_user.id).distinct.limit(20)
        else
          @members = User.joins(:portfolio_accounts).where(approved: true).distinct.limit(20)
        end
      end

      def show
        @user = current_user
        render json: @user
      end
      
      def avatars
        images = AppImage.where("name LIKE?", "avatar-%")
        render json: images.to_json
      end
      
      def states
        states = YAML.load(File.read(File.expand_path("#{Rails.root}/config/states.yml", __FILE__)))
        render json: states.to_json
      end

      def update
      end

      def user
        render json: current_user.to_json
      end
      
      def departments
        render json: current_user.departments.to_json
      end
      
      def industries
        render json: current_user.target_industries.to_json
      end
      
      def update_industries
        delete_industries = current_user.target_industries.map(&:id) - params[:industry_ids]
        UserIndustry.where(user_id: current_user.id, target_industry_id: delete_industries).delete_all
        ActiveRecord::Base.transaction do
          params[:industry_ids].each do |id|
            UserIndustry.create(user_id: current_user.id, target_industry_id: id) unless current_user.target_industries.exists?(id)
          end
        end
        render json: {message: "#{current_user.target_industries.reload.size} industries added!"}.to_json
      end
      
      def update_departments
        delete_industries = current_user.departments.map(&:id) - params[:department_ids]
        UserDepartment.where(user_id: current_user.id, department_id: delete_industries).delete_all
        ActiveRecord::Base.transaction do
          params[:department_ids].each do |id|
            UserDepartment.create(user_id: current_user.id, department_id: id) unless current_user.departments.exists?(id)
          end
        end
        render json: {message: "#{current_user.departments.reload.size} deparments added!"}.to_json
      end
      
    end
  end
end
