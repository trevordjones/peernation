module Api
  module V1
    class ReferralsController < ApplicationController
      include ActionView::Helpers::TextHelper
      def create
        emails = /^[\W]*([\w+\-.%]+@[\w\-.]+\.[A-Za-z]{2,4}[\W]*,{1}[\W]*)*([\w+\-.%]+@[\w\-.]+\.[A-Za-z]{2,4})[\W]*$/.match(params[:emails])
        error_message = "Sorry, we couldn't email one or more of your colleagues. Please be sure to comma separate the emails."
        if !emails.nil?
          begin
            emails = emails[0].split(", ").uniq
            emails.each do |email|
              Referral.create!(referrer_id: current_user.id, status: 'made', email: email)
              Referral.create!(referrer_id: current_user.id, status: 'pending', email: email)
              ReferralMailer.send_referrals(current_user, emails[0], params[:text]).deliver_now
              current_user.update(approved: true)
            end
            render json: "Your referrals have been sent!".to_json
          rescue => e
            Rails.logger.info e
            render json: error_message.to_json, status: :unprocessable_entity
          end
        else
          render json: error_message.to_json, status: :unprocessable_entity
        end
      end
      
      def index
        @portfolio = current_user.portfolio_accounts.map(&:company)
        @target = current_user.target_accounts.map(&:company)
      end
    end
  end
end
