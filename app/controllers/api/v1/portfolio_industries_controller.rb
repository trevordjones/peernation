module Api
  module V1
    class PortfolioIndustriesController < ApplicationController
      def index
        #User this query when we have enough members to better fill the industry filter on the member page
        # @industries = Company.joins(:portfolio_users).includes(:comp_industries).map(&:comp_industries).flatten.uniq
        @industries = Industry.all
      end
    end
  end
end
