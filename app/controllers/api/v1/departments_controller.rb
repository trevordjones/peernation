module Api
  module V1
    class DepartmentsController < ApplicationController
      def index
        render json: Department.all
      end
    end
  end
end
