class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  layout 'landing_page', only: [:linkedin]

  def linkedin
    @user = User.from_omniauth(linkedin_auth)
    if @user.nil?
      resource = User.new(fill_form)
      if resource.save
        sign_in_and_redirect resource, :event => :authentication
        set_flash_message(:success, :success, :kind => "LinkedIn") if is_navigational_format?
      else
        flash[:danger] = "Something went wrong! Please try again"
        render template: 'devise/registrations/new', layout: 'application', locals: {resource: resource}
      end
    else
      set_linkedin_session_variables
      sign_in_and_redirect @user, :event => :authentication
      set_flash_message(:success, :success, :kind => "LinkedIn") if is_navigational_format?
    end
  end

  private

  def after_sign_in_path_for(resource)
    new_portfolio_path
  end

  def set_linkedin_session_variables
    session[:linkedin_token] = linkedin_auth.credentials.token
    session[:linkedin_uid] = linkedin_auth.uid
    session[:linkedin_session] = true
  end

  def linkedin_auth
    request.env["omniauth.auth"]
  end

  def fill_form
    {
      full_name: linkedin_auth.info.name,
      email: linkedin_auth.info.email,
      phone_number: linkedin_auth.info.phone,
      linkedin_url: linkedin_auth.info.urls.public_profile,
      position: linkedin_auth.info.headline,
      city: linkedin_auth.info.location,
      state: linkedin_auth.info.state,
      uid: linkedin_auth.uid,
      provider: linkedin_auth.provider,
      linkedin_photo: linkedin_auth.info.image,
      password: Devise.friendly_token[0,20]
    }
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :phone_number, :linkedin_url, :position, :summary, :password, :password_confirmation, :uid, :provider)
  end

end
