class UsersController < Devise::RegistrationsController
  prepend_before_filter :authenticate_scope!, only: [:edit, :update, :destroy, :edit_profile, :submitted, :profile]
  before_filter :set_avatars, :set_states, only: [:new, :edit, :update, :update_confirmation, :new_profile]
  before_filter :focus_on_industry_ids, only: [:edit]
  layout 'submission', only: [:new, :submitted, :new_profile]

  def new
    build_resource({})
    resource.user_industries.build
    resource.user_departments.build
    set_minimum_password_length
    yield resource if block_given?
    respond_with self.resource
  end

  def new_profile
  end

  def create_profile
    if current_user.update(create_profile_params)
      build_associations
      redirect_to profile_path
    else
      binding.pry
    end
  end

  def create
    build_resource(sign_up_params)
    resource.skip_confirmation!
    resource.terms_of_use = true
    resource.save if resource.errors.empty?
    yield resource if block_given?
    if resource.errors.empty? && resource.persisted?
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_flashing_format?
        sign_up(resource_name, resource)
        handle_referral(resource)
        respond_with resource, location: after_sign_up_path_for(resource)
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_flashing_format?
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
      respond_with resource
    end
  end

  def edit
    set_states
    super
  end

  def submit_application
    if current_user.update(submitted: true)
      redirect_to controller: 'users', action: 'submitted', submission: 'true'
    else
      flash[:danger] = 'Something went wrong! Please submit again.'
      redirect_to profile_path
    end
  end

  def submitted
    if params[:submission]
      current_user.update(submitted: true, approved: true)
      render :submitted
    else
      redirect_to profile_path
    end
  end

  def focus_on_industry_ids
    @focus_on_industry_ids = current_user.target_industries.map(&:id)
  end

  def profile
  end

  def update
    if resource.update(avatar: params[:avatar])
      if update_resource_without_password(resource, account_update_params)
        render json: {user: current_user, message: "Your profile is updated!"}.to_json
      else
        render json: {user: current_user, message: resource.errors.full_messages}.to_json, status: :unprocessable_entity
      end
    end
  end

  def edit_password
  end

  def update_password
    resource_updated = update_resource(current_user, account_update_params)
    if resource_updated
      sign_in current_user, :bypass => true
      flash[:notice] = "Password updated!"
      redirect_to profile_path
    else
      flash[:danger] = current_user.errors.full_messages
      render :edit_password
    end
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  private

  def handle_referral(user)
    token = params[:referrer]
    referrer = ReferralResource.find_by(token: token)
    if referrer
      referral = Referral.where(email: user.email, status: 'pending', referrer_id: referrer.user.id).first
      referral.update(status: 'accepted', referred_id: user.id) unless referral.nil?
    end
  end

  def set_avatars
    @images = AppImage.where("name LIKE?", "avatar-%")
  end

  def set_states
    @states = YAML.load(File.read(File.expand_path("#{Rails.root}/config/states.yml", __FILE__)))
  end

  def after_sign_up_path_for(resource)
    new_portfolio_path
  end

  def after_update_path_for(resource)
    if resource.approved
      profile_path
    else
      new_portfolio_path
    end
  end

  def update_resource_without_password(resource, params)
    resource.update_without_password(params)
  end

  def build_associations
    build_user_industries
    build_target_industries
    # build_departments
  end

  def build_user_industries
    current_user.user_industries.delete_all
    industry_ids = params[:user][:industries][:industry_id].reject {|id| id.blank?}
    industry_ids.each do |id|
      UserIndustry.create(user_id: current_user.id, industry_id: id)
    end
  end

  def build_target_industries
    current_user.target_industries.delete_all
    industry_ids = params[:user][:target_industries][:industry_id].reject {|id| id.blank?}
    industry_ids.each do |id|
      UserIndustry.create(user_id: current_user.id, target_industry_id: id)
    end
  end

  def build_departments
    current_user.departments.delete_all
    department_ids = params[:user][:departments][:department_id].reject{|id| id.empty?}
    department_ids.each do |id|
      UserDepartment.create(user_id: current_user.id, department_id: id)
    end
  end

  def authenticate_scope!
    if current_admin
      redirect_to admin_new_requests_path
    else
      super
    end
  end

  def create_profile_params
    params.require(:user).permit(:role, :currently_sell, :city, :state)
  end

end
