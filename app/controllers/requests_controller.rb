class RequestsController < ApplicationController
  before_filter :authenticate_user!, :approved_user

  def index
    @requested_connections = current_user.requested_connections
  end

end
