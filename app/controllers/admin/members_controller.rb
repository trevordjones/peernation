class Admin::MembersController < ApplicationController
  before_filter :authorize_admin
  layout 'admin_dashboard'

  def index
    @members = User.where(approved: false, rejected: false).paginate(per_page: 1, page: params[:page])
  end

  def new_requests
    @members = User.where(created_at: 2.weeks.ago...Date.tomorrow)
  end
  
  def unapproved
    @members = User.where(approved: false, rejected: false, submitted: true)
  end

  def rejected
    @members = User.where(rejected: true)
  end

  def current
    @members = User.where(approved: true, active: true)
  end

  def show
    @member = User.find_by(id: params[:id])
    @next = User.where("id > ?", @member.id).order("id ASC").first || User.first
    @back = User.where("id < ?", @member.id).order("id DESC").first || User.last
  end

  def new
    @admin = Admin.new
  end

  def create
    @admin = Admin.new(admin_params)
    if @admin.save
      flash[:success] = ["Admin created!"]
      redirect_to new_admin_member_path
    else
      flash.now[:danger] = @admin.errors.full_messages
      render :new
    end
  end

  def suspend_member
    @member = User.find(params[:id])
    @next = User.where("id > ?", @member.id).order("id ASC").first || User.first
    @back = User.where("id < ?", @member.id).order("id DESC").first || User.last
    # MemberStatusMailer.suspended_by_admin(@member).deliver_now
    @member.suspend_date = Time.now
    if @member.save
      flash[:success] = "Member Suspended"
      redirect_to admin_new_requests_path
    else
      flash.now[:danger] = "Oops, something went wrong, try that again."
      render :show
    end
  end

  def approve_member
    @member = User.find(params[:id])
    @next = User.where("id > ?", @member.id).order("id ASC").first || User.first
    @back = User.where("id < ?", @member.id).order("id DESC").first || User.last
    # MemberStatusMailer.approved_by_admin(@member).deliver_now
    @member.approved = true
    if @member.save
      flash[:success] = "Member approved"
      redirect_to admin_member_path(@member.id)
    else
      flash.now[:danger] = "Oops, something went wrong, try that again."
      render :show
    end
  end

  def disapprove_member
    @member = User.find(params[:id])
    @next = User.where("id > ?", @member.id).order("id ASC").first || User.first
    @back = User.where("id < ?", @member.id).order("id DESC").first || User.last
    # MemberStatusMailer.disapproved_by_admin(@member).deliver_now
    @member.rejected = true
    if @member.save
      flash[:success] = "Member rejected"
      redirect_to admin_new_requests_path
    else
      flash.now[:danger] = "Oops, something went wrong, try that again"
      render :show
    end
  end

  def admin_params
    params.require(:admin).permit(:email, :password, :password_confirmation)
  end

end
