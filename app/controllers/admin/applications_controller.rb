class Admin::ApplicationsController < ApplicationController
  before_filter :authorize_admin
  layout 'admin_dashboard'

  def index
    @applicants = User.where(approved: false)
    @applicants = @applicants.by_name(params["name"]) if params["name"].present?
    @applicants = @applicants.by_start_date(params["date"]) if params["date"].present?
  end

  def show
    @applicant = User.find(params[:id])
  end

  def update
    @applicant = User.find(params[:id])
    if params["commit"] == "Approve"
      @applicant.update(approved: true)
      if @applicant.save
        redirect_to admin_applications_path
      else
        flash.now[:error] = "Oops, something went wrong, try that again."
        render :show
      end
   # # else
    #   #ask bob what he'd like done here. May add a RejectedApplicants model and I'll fill with their attributes, then delete from Users
    end
  end


end
