class Admin::IndustriesController < ApplicationController
  before_filter :authorize_admin
  layout 'admin_dashboard'

  def new
    @industry = Industry.new
  end

  def create
    @industry = Industry.new(industry_params)
    if industry.save
      flash[:success] = "Industry saved"
      redirect_to new_admin_industry_path
    else
      flash[:danger] = "Fill in name"
      render :new
    end
  end

  private
  def industry_params
    params.require(:industry).permit(:name)
  end
end
