class Admin::MemberRequestsController < ApplicationController
  before_filter :authorize_admin
  layout 'admin_dashboard'

  def index
    @member_requests = MemberRequest.find_each(batch_size: 100)
  end

  def destroy
    member = MemberRequest.find(params[:id])
    MembershipRequestMailer.send_request(request_params).deliver_now unless @message.nil?
    member.destroy
    flash.now = "Member has been notified!"
  end

end
