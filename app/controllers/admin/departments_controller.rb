class Admin::DepartmentsController < ApplicationController
  before_filter :authorize_admin
  layout 'admin_dashboard'

  def new
    @department = Department.new
  end

  def create
    @department = Department.new(department_params)
    if department.save
      redirect_to new_admin_department_path
      flash[:success] = "Department created"
    else
      flash[:danger] = "Fill in name"
      render :new
    end
  end

  private
  def department_params
    params.require(:department).permit(:name)
  end
end
