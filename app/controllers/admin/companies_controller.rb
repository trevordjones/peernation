class Admin::CompaniesController < ApplicationController
  before_filter :authorize_admin
  layout 'admin_dashboard'
  
  def index
    @companies = Company.where(created_at: 2.weeks.ago...Date.tomorrow)
  end
end
