class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_filter :authenticate_user_or_admin
  layout :layout_by_resource

  def layout_by_resource
    if devise_controller?
      'submission'
    else
      'application'
    end
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:account_update) do |u|
      u.permit(:email,
      :password,
      :password_confirmation,
      :current_password,
      :full_name,
      :anonymous_username,
      :city,
      :state,
      :phone_number,
      :position,
      :summary,
      :linkedin_url,
      :referral_fee,
      :role,
      :currently_sell,
      :avatar,
      :linkedin_photo,
      industries_attributes: [industry_id: []],
      target_industries_attributes: [industry_id: []],
      departments_attributes: [department_id: []])
    end

    devise_parameter_sanitizer.for(:sign_up) do |u|
      u.permit(:email,
      :password,
      :password_confirmation,
      :full_name,
      :anonymous_username,
      :city,
      :state,
      :phone_number,
      :position,
      :summary,
      :linkedin_url,
      :uid,
      :provider,
      :role,
      :currently_sell,
      :avatar,
      :linkedin_photo,
      industries_attributes: [industry_id: []],
      target_industries_attributes: [industry_id: []],
      departments_attributes: [department_id: []])
    end
  end

  def layout_landing_page
    layout 'landing_page' if !user_signed_in?
  end

  def approved_user
    return if current_admin
    authenticate_user!
    if !current_user.submitted
      flash[:warning] = "You haven't yet submitted your application."
      redirect_to profile_path
    elsif !current_user.approved
      flash[:warning] = "You must first be approved to access that page."
      redirect_to profile_path
    end
  end

  def authorize_admin
    unless current_admin
      flash[:warning] = "You are not authorized!"
      redirect_to profile_path
    end
  end

  def after_sign_in_path_for(resource)
    if resource.approved
      companies_path
    else
      profile_path
    end
  end

  def after_sign_out_path_for(resource_or_scope)
    login_path
  end

  def authenticate_user_or_admin
    if request.env['PATH_INFO'].include?('admin')
      if current_user
        flash[:warning] = "You are not authorized!"
        redirect_to profile_path
      else
        authenticate_admin!
      end
    else
      return if current_admin
      authenticate_user!
    end
  end

  def user_type
    return current_admin if current_admin
    return current_user if current_user
  end

  def redirect_if_admin
    redirect_to admin_new_requests_path if current_admin
  end

  def read_notification(id)
    note = Notification.find(id)
    note.read = true
    note.save
  end

  def set_states
    @states = YAML.load(File.read(File.expand_path("#{Rails.root}/config/states.yml", __FILE__)))
  end

end
