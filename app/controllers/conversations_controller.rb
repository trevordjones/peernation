class ConversationsController < ApplicationController
  before_filter :approved_user, :redirect_if_admin

  def index
  end

  def new
  end

  def sent
  end

  def archived
  end

  def trashed
  end

  def new_message
    @recipient = User.find(params[:id])
  end

  def show
    @conversation = Conversation.find(params[:id])
    @conversation.read_conversation(current_user)
    read_notification(params[:note_id]) if params[:read]
  end

  def reply
    @conversation = Conversation.find(params[:id])
  end

  def create
    receiver_ids = params[:ids]
    conversation = Conversation.new(subject: params[:subject])
    if SendMessage.new(params[:subject], params[:body], receiver_ids, current_user).start_conversation
      render json: "Your message was sent!".to_json
    else
      render json: "Couldn't send your message. Please try again".to_json, status: :unprocessable_entity
    end
  end

  def send_message
    member = User.find(params[:id])
    if SendMessage.new(params[:subject], params[:body], [member.id], current_user, member).start_conversation
      render json: "Your message was sent!".to_json
    else
      render json: "Be sure you have a Subject and body filled in.".to_json, status: :unprocessable_entity
    end
  end

  def conversation_params
    params.require(:conversation).permit(:subject)
  end

end
