class ConnectionsController < ApplicationController
  before_filter :approved_user

  def index
    @pending_connections = current_user.pending_connections
    @requested_connections = current_user.requested_connections
    @connections = current_user.peers
    @matches = current_user.matches
  end

  def create
    member = User.find(params[:member_id])
    subject = "#{current_user.full_name} has requested you as a peer!"
    if RequestConnection.new(current_user, member, subject, params[:body]).send_request
      flash[:notice] = "Your request was sent!"
    else
      flash[:danger] = "Something happened. Please try your request again."
    end
    redirect_to member_path(member.id)
  end

  def update
    answer = params[:answer] == 'Accept' ? 'accepted' : 'rejected'
    member = User.find(params[:id])
    requested_connection = Connection.where(user_id: member.id, peer_id: current_user.id, status: "requested").first
    pending_connection = Connection.where(user_id: current_user.id, peer_id: member.id, status: "pending").first
    requested_connection.transaction do
      requested_connection.update(status: answer)
      pending_connection.update(status: answer)
    end
    
    if requested_connection.status == "accepted"
      Notification.create(notified_id: member.id, notifier_id: current_user.id, record_id: current_user.id, notification_type: "connection_accepted")
      ConnectionRequestMailer.accept_request(member, current_user).deliver_now
      flash[:success] = "Congrats! You are now connected with #{member.full_name}."
    else
      Notification.create(notified_id: member.id, notifier_id: current_user.id, record_id: current_user.id, notification_type: "connection_denied")
      flash[:warning] = "You rejected #{member.full_name} as a connection."
    end
    if params[:profile]
      redirect_to member_path(member)
    else
      redirect_to controller: 'connections', action: 'index', show: 'approvals'
    end
  end

end
