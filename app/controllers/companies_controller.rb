class CompaniesController < ApplicationController
  before_filter :authenticate_user_or_admin, :approved_user, except: [:index]
  before_filter :authorize_admin, only: [:edit]
  before_filter :layout_landing_page

  def index
  end

  def new
    @company = Company.new
  end

  def show
    @company = Company.find(params[:id])
    @comments = @company.comments
    @comments = @comments.by_type(params[:type]) if params[:type].present?
    @comments = @comments.by_begin_date(params[:from]) if params[:from].present?
    @comments = @comments.by_end_date(params[:to]) if params[:to].present?
    @comments = Comment.by_contact if params[:comment_type] == "contact"
    @comments = Comment.by_question if params[:comment_type] == "question"
    @comments = Comment.by_comment if params[:comment_type] == "comment"
    params[:keywords] ||= ""
    @comments = Comment.by_keyword(params[:keywords]) if !params[:keywords].empty?
  end

  def create
  end

  def edit
    @company = Company.find(params[:id])
  end

  def update
    @company = Company.find(params[:id])
    @company.update(company_params)
    redirect_to company_path(@company.id)
  end

  def layout_landing_page
    if !user_signed_in? && !admin_signed_in?
      self.class.layout 'companies'
    end
  end
  
  def company_params
    params.require(:company).permit(:logo, :width, :height)
  end

end
