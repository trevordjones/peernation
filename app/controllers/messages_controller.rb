class MessagesController < ApplicationController

  def create
    conversation = Conversation.find(params[:conversation_id])
    if SendMessage.new(conversation.subject, params[:body], params[:ids], current_user, conversation).send_reply_message
      render json: 'Your message was sent!'.to_json
    else
      render json: "Your message wasn't sent. Please try again.".to_json, status: :unprocessable_entity
    end
  end

  def message_params
    params.require(:message).permit(:body, :conversation_id, :sender_id, :receiver_id)
  end

end
