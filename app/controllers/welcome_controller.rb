class WelcomeController < ApplicationController
  skip_before_filter :authenticate_user_or_admin
  before_filter :redirect_from_welcome
  layout 'landing_page'

  def index
    if params[:referrer]
      redirect_to user_omniauth_authorize_path(:linkedin, referrer: params[:referrer])
    end
  end

  def redirect_from_welcome
    if user_signed_in?
      redirect_to profile_path
    end
  end

end
