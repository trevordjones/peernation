class NotificationsController < ApplicationController
  before_filter :approved_user, :redirect_if_admin

  def index
  end
  
  def destroy
    notification = Notification.find(params[:id])
    notification.update(read: true)
    redirect_to notifications_path
  end
end
