module ApplicationHelper

  def navigation_menu
    return 'layouts/admin_navigation' if admin_signed_in?
    return 'layouts/not_approved' if user_signed_in? && !current_user.approved
    return 'layouts/signed_in_navigation' if user_signed_in?
    return 'layouts/signed_out_navigation' if !user_signed_in?
  end

end
