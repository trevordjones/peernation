module UsersHelper
  def photo(member)
    return member.linkedin_photo if current_admin
    return member.linkedin_photo if member == current_user
    if current_user.connected?(member) || current_user.pending?(member)
      member.linkedin_photo
    else
      member.avatar
    end
  end

  def name(member)
    return member.full_name if current_admin
    return member.full_name if member == current_user
    if current_user.connected?(member) || current_user.pending?(member)
      member.full_name
    else
      member.anonymous_username
    end
  end

  def user_type
    return current_admin if current_admin
    return current_user if current_user
  end
  
  def unread_notifications_number
    number = Notification.unread_notifications(current_user).size
    return number unless number.zero?
  end
  
  def unread_messages_number
    number = ReceivedConversation.unopened(current_user).size
    return number unless number.zero?
  end
end
