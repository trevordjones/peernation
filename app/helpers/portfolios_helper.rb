module PortfoliosHelper
  def portfolio_button_text
    if current_user.portfolio_accounts.size == 1
      "Add a company"
    else
      "Add another company"
    end
  end
end
