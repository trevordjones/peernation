class TargetDepartment < ActiveRecord::Base
  belongs_to :department
  belongs_to :target_account
  belongs_to :user
  belongs_to :contact_level
end
