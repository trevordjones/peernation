class TargetAccount < ActiveRecord::Base
  has_many :target_departments, dependent: :destroy
  has_many :departments, through: :target_departments
  has_many :contact_levels, through: :target_departments
  belongs_to :company
  belongs_to :user
  belongs_to :industry
  validates :company_id, presence: true, uniqueness: {scope: :user_id, message: 'has already been added to your targets.'}
  validates :user_id, presence: true
  
  scope :active_targets, -> {where(active: true).size }
end
