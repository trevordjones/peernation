class PortfolioDepartment < ActiveRecord::Base
  belongs_to :department
  belongs_to :portfolio_account
  belongs_to :user
  belongs_to :contact_level
end
