class ReceivedMessage < ActiveRecord::Base
  belongs_to :message
  belongs_to :receiver, class_name: 'User', foreign_key: :receiver_id
  
end
