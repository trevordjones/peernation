class Message < ActiveRecord::Base
  belongs_to :conversation
  belongs_to :sender, class_name: 'User', foreign_key: :sender_id
  has_many :received_messages, dependent: :destroy
  validates :body, presence: true
  validates :conversation_id, presence: true
  
  def receivers
    received_messages.map(&:receiver)
  end
end
