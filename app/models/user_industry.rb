class UserIndustry < ActiveRecord::Base
  belongs_to :user
  belongs_to :industry
  belongs_to :target_industry, class_name: 'Industry', foreign_key: :target_industry_id
  accepts_nested_attributes_for :user, allow_destroy: true, reject_if: :all_blank
end
