class Conversation < ActiveRecord::Base
  has_many :messages, -> {order(created_at: :desc)}, dependent: :destroy
  has_many :sent_conversations, -> {order(created_at: :desc)}, dependent: :destroy
  has_many :received_conversations, -> {order(created_at: :desc)}, dependent: :destroy
  validates :subject, presence: true
  
  def receivers
    received_conversations.map(&:receiver)
  end
  
  def senders
    sent_conversations.map(&:sender)
  end
  
  def participants(user)
    participants = receivers + senders
    participants.uniq.reject{|participant| participant == user}
  end
  
  def read_conversation(user)
    received_convo = received_conversations.find{|rc| rc.receiver == user}
    received_convo.update(opened: true) unless received_convo.nil? || received_convo.opened
  end
end
