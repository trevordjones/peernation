class ContactLevel < ActiveRecord::Base
  has_many :portfolio_departments
  has_many :portfolio_accounts, through: :portfolio_departments
end
