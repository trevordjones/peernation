class Notification < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  #Notification Types
  #connection_requested, connection_accepted, connection_denied, new_message
  belongs_to :notified, class_name: 'User', foreign_key: :notified_id
  belongs_to :notifier, class_name: 'User', foreign_key: :notifier_id
  scope :unread_notifications, -> (user){user.notifications.where(read: false)}
  
  NOTIFICATIONS = {connection_requested: 'user', connection_accepted: 'user', new_message: 'comment'}
  
  def glyphicon
    NOTIFICATIONS[notification_type.to_sym]
  end
  
  def display_text
    send(notification_type.to_sym)
  end
  
  def connection_requested
    "#{notifier.full_name} wants to connect with you"
  end
  
  def connection_accepted
    "#{notifier.full_name} has accepted your peer request"
  end
  
  def new_message
    if notified.connected?(notifier) || notified.pending?(notifier)
      "#{notifier.full_name} sent you a message"
    else
      "#{notifier.anonymous_username} sent you a message"
    end
  end
  
  def link
    link_list[notification_type.to_sym] + "?read=true&note_id=#{id}"
  end
  
  def link_list
    {
      connection_requested: member_path(notifier_id),
      connection_accepted: member_path(notifier_id),
      new_message: inbox_message_path(record_id)
    }
  end
end
