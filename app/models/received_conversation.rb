class ReceivedConversation < ActiveRecord::Base
  belongs_to :conversation
  belongs_to :receiver, class_name: 'User', foreign_key: :receiver_id
  scope :unopened, -> (user){where(opened: false, receiver_id: user.id)}
  scope :opened, -> (user, conversation){where(opened: true, receiver_id: user.id, conversation_id: conversation.id)}
  
  def self.convo_opened?(user, conversation)
    where(opened: false, receiver_id: user.id, conversation_id: conversation.id).empty?
  end
end
