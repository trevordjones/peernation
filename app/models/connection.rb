class Connection < ActiveRecord::Base
  belongs_to :user
  belongs_to :peer, class_name: 'User', foreign_key: :peer_id

  validates_uniqueness_of :user_id, :scope => [:peer_id]

end
