class ReferralResource < ActiveRecord::Base
  belongs_to :user

  before_create :generate_token

  BASE_URL=ENV['REFERRAL_LINK']

  protected

  def generate_token
    self.token = loop do
      random_token = SecureRandom.urlsafe_base64(nil, false)[2...10]
      self.link = BASE_URL + random_token
      break random_token unless ReferralResource.exists?(token: random_token)
    end
  end
end
