class PortfolioAccount < ActiveRecord::Base
  has_many :portfolio_departments, dependent: :destroy
  has_many :departments, through: :portfolio_departments
  has_many :contact_levels, through: :portfolio_departments
  belongs_to :company
  belongs_to :user
  belongs_to :industry
  validates :company_id, presence: true, uniqueness: {scope: :user_id, message: 'has already been added to your portfolio.'}
  validates :user_id, presence: true
end
