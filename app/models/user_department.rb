class UserDepartment < ActiveRecord::Base
  belongs_to :user
  belongs_to :department
  accepts_nested_attributes_for :user, allow_destroy: true, reject_if: :all_blank
end
