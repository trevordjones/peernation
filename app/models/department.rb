class Department < ActiveRecord::Base
  has_many :portfolio_departments
  has_many :portfolio_accounts, through: :portfolio_departments
  has_many :target_accounts
  has_many :company_departments
  has_many :portfolio_companies, through: :portfolio_accounts, source: :company
  has_many :target_companies, through: :target_accounts, source: :company
  has_many :portfolio_users, through: :portfolio_accounts, source: :user
  has_many :target_users, through: :target_accounts, source: :user
  has_many :department_companies, through: :company_departments, source: :company
  has_many :user_departments, inverse_of: :department
  has_many :users, through: :user_departments
end
