class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :confirmable, :rememberable, :trackable, :validatable, :omniauthable, :omniauth_providers => [:linkedin]

  has_many :portfolio_accounts, dependent: :destroy
  has_many :target_accounts, -> {where(active: true)}, dependent: :destroy
  has_many :connections
  has_many :peers, -> { where(connections: {status: 'accepted'})}, through: :connections
  has_many :requested_connections, -> {where(connections: {status: 'requested'})}, source: :peer, through: :connections
  has_many :pending_connections, -> {where(connections: {status: 'pending'})}, source: :peer, through: :connections
  has_many :referrals, foreign_key: :referrer_id
  has_many :referrals_made, -> {where(referrals: {status: 'made'})}, source: :referrer, through: :referrals, foreign_key: :referrer_id
  has_many :referrals_accepted, -> {where(referrals: {status: 'accepted'})}, source: :referred, through: :referrals, foreign_key: :referred_id
  has_one :referral_resource
  has_many :portfolio_companies, through: :portfolio_accounts, source: :company
  has_many :portfolio_industries, through: :portfolio_accounts, source: :industry
  has_many :portfolio_departments, dependent: :destroy
  has_many :target_companies, through: :target_accounts, source: :company
  has_many :target_departments, dependent: :destroy
  
  has_many :active_received_conversations, -> {where(archived: false, trashed: false).order(created_at: :desc)}, class_name: 'ReceivedConversation', foreign_key: :receiver_id
  has_many :active_conversations_received, through: :active_received_conversations, source: :conversation
  
  has_many :archived_received_conversations, -> {where(archived: true).order(created_at: :desc)}, class_name: 'ReceivedConversation', foreign_key: :receiver_id
  has_many :archived_conversations_received, through: :archived_received_conversations, source: :conversation
  
  has_many :sent_conversations, -> {where(archived: false, trashed: false).order(created_at: :desc)}, foreign_key: :sender_id
  has_many :conversations_sent, through: :sent_conversations, source: :conversation
  has_many :archived_sent_conversations, -> {where(archived: true).order(created_at: :desc)}, class_name: 'SentConversation', foreign_key: :sender_id
  has_many :archived_conversations_sent, through: :archived_sent_conversations, source: :conversation
  
  has_many :received_messages, foreign_key: :receiver_id
  has_many :user_industries, foreign_key: :target_industry_id
  has_many :user_industries, inverse_of: :user
  has_many :target_industries, through: :user_industries
  has_many :industries, through: :user_industries
  has_many :user_departments, inverse_of: :user
  has_many :departments, through: :user_departments
  has_many :comments
  has_many :notifications, class_name: 'Notification', foreign_key: :notified_id, dependent: :destroy
  accepts_nested_attributes_for :user_industries, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :user_departments, allow_destroy: true, reject_if: :all_blank

  validates :full_name, presence: true
  validates_uniqueness_of :anonymous_username, allow_nil: true
  validates_uniqueness_of :email
  after_create :generate_referral_resource

  scope :by_name, lambda { |name| where(first_name: name) }
  scope :by_start_date, lambda { |date| where(start_date: date) }
  scope :by_city, lambda { |cities| where(city: cities) }
  scope :by_ids, lambda {|ids| where(id: ids)}

  def self.by_department(ids)
    User.joins(:portfolio_departments).where(portfolio_departments: {department_id: ids})
  end

  def self.by_contact(contacts)
    User.joins(:portfolio_departments).where(portfolio_departments: {contact_level_id: contacts})
  end

  def self.by_industry(ids)
    User.joins(:portfolio_companies).where(portfolio_accounts: {company_id: Company.by_industry(ids).map(&:id)})
  end

  def matches
    accounts = TargetAccount.where(company_id: portfolio_accounts.map(&:company_id))
    accounts += PortfolioAccount.where(company_id: target_accounts.map(&:company_id))
    accounts.to_a.select!{|account| account.user_id != id && !peers.exists?(account.user.id)}
    accounts.map(&:user)
  end

  def self.from_omniauth(auth)
    begin
      user = User.find_by_uid(auth.uid)
      return user if user.nil?
      user.uid = auth.uid
      user.provider = auth.provider
      user.save
      user
    rescue StandardError => e
      Rails.logger.info("Could not find response from Omniauth")
      Rails.logger.info(e.message)
      Rails.logger.info(e.backtrace.join("/n"))
      return nil
    end
  end

  def self.input_params(auth)
    {first_name: auth.info.first_name, last_name: auth.info.last_name, email: auth.info.email, city: auth.extra.raw_info.location.name, phone_number: auth.info.phone, position: auth.info.description, linkedin_url: auth.info.urls.public_profile, uid: auth.uid, provider: auth.provider}
  end

  def city_state
    city + ', ' + state
  end

  def archived_conversations
    archived_conversations_sent + archived_conversations_received
  end

  def trashed_conversations
    Conversation.joins(:sent_conversations).where(sent_conversations: {trashed: true, sender_id: id}) +
    Conversation.joins(:received_conversations).where(received_conversations: {trashed: true, receiver_id: id})
  end
  
  def status
    return "Not submitted" unless submitted
    return "Active" if approved
    return "Rejected" if rejected
    return "Pending" if !approved
  end

  def connected?(member_id)
    peers.exists?(member_id)
  end

  def requested?(member_id)
    requested_connections.exists?(member_id)
  end

  def pending?(member_id)
    pending_connections.exists?(member_id)
  end

  def no_connection(member_id)
    !peers.exists?(member_id) && !requested_connections.exists?(member_id) && !pending_connections.exists?(member_id)
  end
  
  protected
  
  def generate_referral_resource
    ReferralResource.create(user_id: id)
  end
  
end
