class Industry < ActiveRecord::Base
  has_many :portfolio_accounts
  has_many :target_accounts
  has_many :company_industries
  has_many :portfolio_companies, through: :portfolio_accounts, source: :company
  has_many :target_companies, through: :target_accounts, source: :company
  has_many :industry_companies, through: :company_industries, source: :company
  has_many :portfolio_users, through: :portfolio_accounts, source: :user
  has_many :target_users, through: :target_accounts, source: :user
  has_many :user_industries, inverse_of: :industry
  has_many :users, through: :user_industries
  scope :sell_for, -> {where(sell_for: true)}
  scope :focus_on, -> {where(focus_on: true)}
  # scope :by_id, lambda do |id| { |id| where(id: id) }

  def self.by_id(ids)
    ids.map {|id| Industry.find(id)}
  end
end
