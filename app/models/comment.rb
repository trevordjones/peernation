class Comment < ActiveRecord::Base
  has_ancestry

  scope :by_begin_date, lambda { |date| where( created_at: Date.parse(date).beginning_of_day...end_of_day) }
  belongs_to :user
  scope :by_end_date, lambda { |date| where(:created_at < date.to_date) }
  scope :by_type, lambda { |type| where(comment_type: type) }
  validates :content, presence: true
  validates :author, presence: true
  validates :comment_type, presence: true
  
  def self.by_begin_date(date)
    date = Date.parse(date)
    where("created_at > ? and ancestry is null", date.beginning_of_day)
  end

  def self.by_end_date(date)
    date = Date.parse(date)
    where("created_at < ? and ancestry is null", date.end_of_day)
  end

  def self.by_contact
    where("comment_type = ? and ancestry is null", "Contact")
  end

  def self.by_question
    where("comment_type = ? and ancestry is null", "Question")
  end

  def self.by_comment
    where("comment_type = ? and ancestry is null", "Comment")
  end

  def self.by_keyword(keyword)
    where("lower(content) LIKE :keyword OR first_name LIKE :keyword OR last_name LIKE :keyword OR title LIKE :keyword OR email LIKE :keyword", keyword: "%#{keyword}%")
  end

  def show_image(admin, current_user, user)
    if admin
      user.linkedin_photo
    elsif current_user == user || current_user.connected?(user)
      user.linkedin_photo
    else
      user.avatar
    end
  end

  def show_name(admin, current_user, user)
    if admin
      user.full_name
    elsif current_user == user || current_user.connected?(user)
      user.full_name
    else
      user.anonymous_username
    end
  end

end
