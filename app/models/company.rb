class Company < ActiveRecord::Base
  has_many :portfolio_accounts
  has_many :target_accounts
  has_many :company_industries
  has_many :company_departments
  has_many :portfolio_users, through: :portfolio_accounts, source: :user
  has_many :target_users, through: :target_accounts, source: :user
  has_many :portfolio_departments, dependent: :destroy
  has_many :target_departments, through: :target_accounts, source: :department
  has_many :portfolio_industries, through: :portfolio_accounts, source: :industry
  has_many :target_industries, through: :target_accounts, source: :industry
  has_many :comp_industries, through: :company_industries, source: :industry
  has_many :comp_departments, through: :company_departments, source: :department
  has_many :comments, :as => :commentable, :dependent => :destroy
  mount_uploader :logo, CompanyLogoUploader
  validates :city, presence: true
  validates :state, presence: true
  validates :name, presence: true

  scope :by_name, lambda { |name| where(name: name) }
  scope :by_city, lambda { |cities| where(city: cities) }

  def self.by_industry(ids)
    Company.joins(:comp_industries).where(company_industries: {industry_id: ids})
  end

  def contacts(user)
    all_contacts = portfolio_accounts.select {|account| user.id == account.user_id}.map {|account| account.contact_levels}
    all_contacts.join(",")
  end

  def image_size
    image_ratio = width.to_f / height.to_f
    return "square" if image_ratio < 1.5
    return "medium" if image_ratio < 2.0
    return "rectangle" if image_ratio > 2.0
  end

end
