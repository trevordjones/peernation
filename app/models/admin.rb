class Admin < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
         
  def connected?(member_id)
    true
  end

  def requested?(member_id)
    false
  end

  def pending?(member_id)
    false
  end

  def no_connection(member_id)
    false
  end
end
