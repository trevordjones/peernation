class FindCompany
  attr_accessor :id, :name, :city, :state, :industry_ids, :department_ids, :contact_level_ids, :new_company
  
  def initialize(company)
    @id = company[:id]
    @name = company[:name]
    @city = company[:city]
    @state = company[:state]
    @industry_ids = company[:industry_ids]
    @department_ids = company[:department_ids]
    @contact_level_ids = company[:contact_level_ids]
    @new_company = company[:new_company]
  end
  
  def create_portfolio
    if id
      company = Company.find(id)
      validate_portfolio_params(company)
    else
      begin
        company = Company.new(name: name, city: city, state: state)
        validate_company(company)
        raise 'error' unless company.errors.empty?
        company.save!
        industries = Industry.find(industry_ids)
        industries.each do |industry|
          CompanyIndustry.create(company_id: company.id, industry_id: industry.id)
        end
      rescue => e
        return company
      end
    end
    return company
  end
  
  private
  
  def validate_portfolio_params(company)
    company.errors.add(:departments, 'must be selected') unless department_ids
    company.errors.add(:contacts, 'must be selected') unless contact_level_ids
  end
  
  def validate_company(company)
    company.errors.add(:industries, 'must be selected') if !industry_ids && new_company
    company.errors.add(:name, "can't be blank") if name.empty?
    if new_company
      company.errors.add(:state, "must be selected") unless state
      company.errors.add(:city, "can't be blank") unless city
    end
    validate_portfolio_params(company)
  end
end
