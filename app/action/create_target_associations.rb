class CreateTargetAssociations
  
  def initialize(target, current_user, company_params)
    @target = target
    @current_user = current_user
    @department_ids = company_params[:department_ids]
    @contact_level_ids = company_params[:contact_level_ids]
  end
  
  def create_target_associations
    if target_valid?
      target.active = current_user.target_accounts.active_targets < 5
      if target.save
        create_department_associations
        create_contact_level_associations
      end
    end
    target.errors.empty?
  end
  
  def create_department_associations
    department_ids.each do |id|
      TargetDepartment.create(user_id: current_user.id, department_id: id, target_account_id: target.id)
    end
  end
  
  def create_contact_level_associations
    contact_level_ids.each do |id|
      TargetDepartment.create(user_id: current_user.id, contact_level_id: id, target_account_id: target.id)
    end
  end
  
  def target_valid?
    target.errors.add(:departments, 'must be selected') if department_ids.nil?
    target.errors.add(:contact_levels, 'must be selected') if contact_level_ids.nil?
    target.errors.empty?
  end
  
  private
  
  attr_accessor :target, :current_user, :department_ids, :contact_level_ids
  
end
