class SendMessage
  
  def initialize(subject, body, receiver_ids, current_user, conversation = nil)
    @subject = subject
    @body = body
    @receiver_ids = receiver_ids
    @current_user = current_user
    @member = member
    @conversation = conversation
  end

  def start_conversation
    conversation = Conversation.new(subject: subject)
    begin
      ActiveRecord::Base.transaction do
        if conversation.save!
          message = Message.new(body: body, conversation_id: conversation.id, sender_id: current_user.id)
          SentConversation.create!(conversation_id: conversation.id, sender_id: current_user.id)
          receiver_ids.each do |receiver_id|
            if message.save!
              ReceivedMessage.create!(message_id: message.id, receiver_id: receiver_id)
              ReceivedConversation.create!(conversation_id: conversation.id, receiver_id: receiver_id)
              # Notification.create!(notified_id: receiver_id, notifier_id: current_user.id, notification_type: 'new_message', record_id: conversation.id)
            end
          end
        end
      end
      true
    rescue
      false
    end
  end
  
  def send_reply_message
    message = Message.new(body: body, conversation_id: conversation.id, sender_id: current_user.id)
    begin
      ActiveRecord::Base.transaction do
        if message.save!
          if !conversation.senders.include?(current_user)
            SentConversation.create!(conversation_id: conversation.id, sender_id: current_user.id)
          end
          new_receivers.each do |new_receiver|
            ReceivedConversation.create!(conversation_id: conversation.id, receiver_id: new_receiver)
          end
          ReceivedConversation.where(conversation_id: conversation.id, receiver_id: receiver_ids).update_all(opened: false)
          receiver_ids.each do |receiver_id|
            ReceivedMessage.create!(message_id: message.id, receiver_id: receiver_id)
            # Notification.create!(notified_id: receiver_id, notifier_id: current_user.id, notification_type: 'new_message', record_id: conversation.id)
          end
        end
        return true
      end
    rescue
      return false
    end
  end

  private
  attr_accessor :subject, :body, :receiver_ids, :current_user, :member, :conversation
  
  def new_receivers
    receiver_ids.select{|id| !conversation.receivers.map(&:id).include?(id)}
  end
end
