class RequestConnection
  def initialize(current_user, member, subject, body)
    @current_user = current_user
    @member = member
    @subject = subject
    @body = body
  end
  
  def send_request
    begin
      ActiveRecord::Base.transaction do
        if SendMessage.new(subject, body, [member.id], current_user).start_conversation
          Connection.create!(user_id: current_user.id, peer_id: member.id, status: 'requested')
          Connection.create!(user_id: member.id, peer_id: current_user.id, status: 'pending')
          Notification.create!(notified_id: member.id, notifier_id: current_user.id, record_id: current_user.id, notification_type: 'connection_requested')
          # ConnectionRequestMailer.send_request(current_user, member).deliver_now
        end
        return true
      end
    rescue
      return false
    end
  end
  
  private
  
  attr_accessor :current_user, :member, :subject, :body, :conversation
end
