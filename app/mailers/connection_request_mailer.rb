class ConnectionRequestMailer < ApplicationMailer

  def send_request(requester, requestee)
    @requester = requester
    @requestee = requestee
    mail(to: @requestee.email, subject: "#{@requester.full_name} has requested to connect with you")
  end

  def accept_request(requester, requestee)
    @requester = requester
    @requestee = requestee
    mail(to: @requester.email, subject: "Your request to #{@requestee.full_name} was accepted!")
  end
end
