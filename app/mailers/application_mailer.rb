class ApplicationMailer < ActionMailer::Base
  include Roadie::Rails::Automatic
  default from: "bob.flores@peernation.co"
  layout 'mailer'

  add_template_helper(EmailHelper)
end
