class MemberStatusMailer < ApplicationMailer

  def approved_by_admin(user)
    @user = user
    @url = 'http://localhost:3000/login'
    mail(to: @user.email, subject: "Good news, You're Approved!")
  end

  def disapproved_by_admin(user)
    @user = user
    mail(to: @user.email, subject: "Ah pees, you done got rejected.")
  end

  def suspended_by_admin(user)
    @user = user
    mail(to: @user.email, subject: "You've been suspended.")
  end

end
