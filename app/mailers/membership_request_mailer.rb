class MembershipRequestMailer < ApplicationMailer

  def send_request(user_info)
    @name = user_info[:first_name]
    mail(to: user_info[:email], subject: "Peer Nation - Coming Soon!")
  end

  def send_to_peernation(user_info)
    @name = user_info[:first_name] + " " + user_info[:last_name]
    @email = user_info[:email]
    mail(to: "bob.flores@me.com", subject: "New Membership Request")
  end

end
