class ReferralMailer < BaseMandrillMailer
  
  def send_referrals(user, email, content)
    # subject = "You've been invited to join Peer Nation"
    # merge_vars = {
    #   "CONTENT" => content,
    # }
    # body = mandrill_template("referral-email", merge_vars)
    #
    # send_mail(email, subject, body)
    @user = user
    @content = content
    mail(to: email, from: user.email, subject: "You've been invited to join Peer Nation")
  end
  
end
