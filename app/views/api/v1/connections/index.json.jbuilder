json.connections @connections.each do |c|
  json.id c.id
  json.email c.email
  json.full_name c.full_name
  json.linkedin_photo c.linkedin_photo
end