json.recipients @recipients.each do |r|
  json.id r.id
  json.name name(r)
  json.photo photo(r)
  json.email r.email
end
