json.companies @companies.each do |company|
  json.id company.id
  json.name company.name
  json.size company.size
  json.city company.city
  json.state company.state
  json.logo company.logo
  json.num_of_users company.num_of_users
  json.num_of_comments company.num_of_comments
  json.width company.width
  json.height company.height
end
