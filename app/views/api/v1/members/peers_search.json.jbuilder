json.members @members do |member|
  json.partial! partial: 'members', locals: {member: member}
end