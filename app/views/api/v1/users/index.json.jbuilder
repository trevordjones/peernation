json.members @members do |member|
  json.id member.id
  json.name name(member)
  json.city member.city
  json.state member.state
  json.position member.position
  json.currently_sell member.currently_sell
  json.photo photo(member)
  json.role member.role
  json.portfolio_count member.portfolio_accounts.size
  json.portfolio member.portfolio_accounts.each do |portfolio|
    json.industry_names portfolio.company.comp_industries.limit(2).map(&:name).join(", ")
    json.department_names portfolio.departments.limit(2).map(&:name).join(", ")
    json.id portfolio.id
    json.name portfolio.company.name
    json.industries portfolio.company.comp_industries.each do |industry|
      json.id industry.id
      json.name industry.name
    end
    json.departments portfolio.departments.each do |dep|
      json.id dep.id
      json.name dep.name
    end
    json.contacts portfolio.contact_levels.each do |contact|
      json.id contact.id
      json.name contact.name
    end
  end
end
