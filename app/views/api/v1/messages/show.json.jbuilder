json.conversation @conversation
json.messages @conversation.messages.each do |message|
  json.message_id message.id
  json.body message.body
  json.created_at message.created_at
  json.sender do
    json.id message.sender.id
    json.name name(message.sender)
    json.photo photo(message.sender)
  end
end

json.receivers @conversation.participants(current_user).each do |receiver|
    json.id receiver.id
    json.name name(receiver)
    json.photo photo(receiver)
end

json.new_sender do
  json.id current_user.id
  json.name current_user.full_name
  json.photo current_user.linkedin_photo
end
