json.received @received.each do |conversation|
  json.id conversation.id
  json.subject conversation.subject
  json.senders conversation.sent_conversations.each do |sc|
    json.name name(sc.sender)
    json.linkedin_photo photo(sc.sender)
  end
  json.opened ReceivedConversation.convo_opened?(current_user, conversation)
  json.sender_names conversation.senders.map{|sender| name(sender)}.join(", ")
  json.message conversation.messages.first
end
json.total_pages @received.total_pages
