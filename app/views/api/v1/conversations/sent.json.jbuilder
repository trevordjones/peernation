json.sent @sent.each do |conversation|
  json.id conversation.id
  json.subject conversation.subject
  json.receivers conversation.received_conversations do |rc|
    json.receiver_name name(rc.receiver)
    json.linkedin_photo photo(rc.receiver)
  end
  json.receiver_names conversation.receivers.map(&:full_name).join(", ")
  json.message conversation.messages.first
end
json.total_pages @sent.total_pages
