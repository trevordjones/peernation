json.trashed @trashed.each do |conversation|
  json.id conversation.id
  json.subject conversation.subject
  json.receivers conversation.received_conversations.each do |rc|
    json.name name(rc.receiver)
    json.photo photo(rc.receiver)
  end
  json.opened ReceivedConversation.convo_opened?(current_user, conversation)
  json.receiver_names conversation.receivers.map{|receiver| name(receiver)}.join(", ")
  json.message conversation.messages.first
end
json.total_pages @trashed.total_pages
