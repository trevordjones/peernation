json.sent_conversations @sent_conversations.each do |conversation|
  json.id conversation.id
  json.subject conversation.subject
  json.body conversation.body
  json.receiver_id conversation.receiver.id
  json.receiver_name name(conversation.receiver)
  json.linkedin_photo  photo(conversation.receiver)
  json.messages conversation.messages
end

json.received_conversations @received_conversations.each do |conversation|
  json.id conversation.id
  json.subject conversation.subject
  json.body conversation.body
  json.sender_id conversation.sender.id
  json.sender_name name(conversation.sender)
  json.linkedin_photo  photo(conversation.sender)
  json.messages conversation.messages
end

json.archived_conversations @archived_conversations.each do |convo|
  json.id convo.id
  json.subject convo.subject
  json.body convo.body
  json.receiver_name convo.receiver.full_name
  json.sender_name name(convo.sender)
  json.current_user current_user.full_name
  json.receiver_name name(convo.receiver)
  json.receiver_photo  photo(convo.receiver)
  json.sender_photo photo(convo.sender)
  json.messages convo.messages
end

json.trashed_conversations @trashed_conversations.each do |convo|
  json.id convo.id
  json.subject convo.subject
  json.body convo.body
  json.receiver_id convo.receiver.id
  json.sender_name name(convo.sender)
  json.receiver_name name(convo.receiver)
  json.current_user current_user.full_name
  json.receiver_photo photo(convo.receiver)
  json.sender_photo photo(convo.sender)
  json.messages convo.messages
end

json.total_sent_pages @sent_conversations.total_pages
json.total_received_pages @received_conversations.total_pages
json.total_archived_pages @archived_conversations.total_pages
json.total_trashed_pages @trashed_conversations.total_pages
