json.notifications @notifications do |note|
  json.id note.id
  json.notified_id note.notified_id
  json.notifier_id note.notifier_id
  json.notification_type note.notification_type
  json.link note.link
  json.text note.display_text
  json.photo photo(note.notifier)
  json.glyphicon note.glyphicon
end
