(function(window){

  $("#request-as-peer").click(function(){
    requestMessage();
  });
  
  $("#send-message").click(function(){
    peerMessage();
  });
  
  $(".trash-message").click(function(){
    if ($("#peer-request").is(":visible")){
      $("#peer-request").toggle();
    } else if ($("#peer-message").is(":visible")){
      $("#peer-message").toggle();
    }
  })
  
  function requestMessage(){
    if ($("#peer-message").is(":visible")){
      $("#peer-message").toggle();
    }
    $("#peer-request").toggle();
  }
  
  function peerMessage(){
    if ($("#peer-request").is(":visible")){
      $("#peer-request").toggle();
    }
    $("#peer-message").toggle();
  }

})(window);
