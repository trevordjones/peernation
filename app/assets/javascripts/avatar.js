(function(window){
  var avatarUrl;
  var imageId;
  window.addAvatar = function(){
    if (avatarUrl === undefined) {
      $(".avatar-notice").removeClass('hide')
    } else {
      $("#avatar-input").val(avatarUrl);
      $("#avatarModal").modal('hide');
      $("#avatar-image").attr("src", avatarUrl).addClass("selected-avatar");
    };
  };

  window.assignAvatar = function(user, url, id){
    if (imageId){
      $("#avatar-" + imageId).removeClass("selected-avatar-modal");
      $(".green-" + imageId).addClass("hide")
      $("#avatar-" + id).addClass("selected-avatar-modal");
      $(".green-" + id).removeClass("hide");
      imageId = id;
      avatarUrl = url;
    } else {
      avatarUrl = url;
      imageId = id;
      avatarSelector = "#avatar-" + imageId;
      checkSelector = ".green-" + imageId;
      $(avatarSelector).addClass("selected-avatar-modal");
      $(checkSelector).removeClass("hide");
    }
  };
  
  var reassignAvatar = function(){
    avatarUrl = $("#avatar_url").attr("data-url")
    if (avatarUrl !== ""){
      $("#avatar-image").attr("src", avatarUrl).addClass("selected-avatar");
    }
  };
  
  var passwordOnKeyup = function(){
    var password;
    var password_confirmation;
    $("#user_password").keyup(function(){
      password = $(this).val();
      if (password.length > 7){
        $(this).removeClass("password-invalid");
        $(this).addClass("password-valid");
      } else if (password.length < 8){
        $(this).addClass("password-invalid");
        $(this).removeClass("password-valid");
      }
    })
    $("#user_password_confirmation").keyup(function(){
      password_confirmation = $(this).val();
      if (password === password_confirmation){
        $(this).removeClass("password-invalid");
        $(this).addClass("password-valid");
      } else {
        $(this).removeClass("password-valid")
        $(this).addClass("password-invalid")
      }
    })
  }
  
  var passwordOnFocus = function(){
    var password
    var password_confirmation
    $("#user_password").focus(function(){
      password = $(this)
      if (password.val().length < 8){
        password.addClass("password-invalid")
      } else {
        password.addClass("password-valid")
      }
    })
    $("#user_password_confirmation").focus(function(){
      password = $("#user_password")
      password_confirmation = $(this)
      if (password_confirmation.val() === password.val()){
        password_confirmation.addClass("password-valid");
        password_confirmation.removeClass("password-invalid")
      } else {
        password_confirmation.addClass("password-invalid");
        password_confirmation.removeClass("password-valid");
      }
    })
  }
  
  passwordOnKeyup();
  passwordOnFocus();
  
  reassignAvatar();
})(window);
