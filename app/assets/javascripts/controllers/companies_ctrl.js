'use strict';

var app = angular.module('app')

app.controller("CompaniesController", ["$scope", "$http", "$window", "$location", "$uibModal", "Data", function($scope, $http, $window, $location, $uibModal, Data){

  var company_id;
  
  Data.allIndustries().success(function(response){
    $scope.industries = response;
  })

  Data.allCompanies(location.search).success(function(response){
    $scope.companies = response.companies;
    $scope.numAllCompanies = response.companies.length;
  })

  var companiesByLocation = [], industryIds = [], companyIds = [], searchUrl = "/api/v1/company_search", city;
  $scope.cities = []

  function locations(companyCities){
    var allCities = companyCities.map(function(company){return company.city + ", " + company.state});
    var unique = allCities.filter(uniqueCities)
    $scope.locations = unique;
  }

  function uniqueCities(value, index, self){
    return self.indexOf(value) === index;
  }

  $scope.isRectangleImage = function(width, height){
    return (width / height) > 2.0
  }

  $scope.isSquareImage = function(width, height){
    return (width / height) < 1.5;
  }
  
  $scope.isMediumImage = function(width, height){
    return ((width / height) > 1.5) && ((width / height) < 2.0);
  }

  $scope.getLocation = function(city){
    return $http.get('/api/v1/company_city_search', {
      params: {
        city: city
      }
    }).then(function(response){
      $scope.noResults = false;
      if (response.data.length === 0) {
        $scope.noResults = true;
      }
      return response.data;
    })
  }

  $scope.clearCitySearch = function(){
    $scope.locationSelected = null;
    $scope.noResults = false;
  }

  $scope.clearSearch = function(){
    $scope.clearCitySearch();
    $scope.cities = [];
    industryIds = [];
    angular.forEach($scope.industries, function(industry){
      industry.checked = false;
    })
    doASearch();
  }

  function byCity(city){
    return function(company){
      return company.city == city;
    }
  }

  $scope.companyLimit = 20;
  $scope.loadMore = function(){
    $scope.companyLimit += 20
  }

  function doASearch(){
    $scope.inputsDisabled = true;
    searchUrl = '/api/v1/company_search' + "?industry_ids=[" + industryIds + "]&cities=[" + $scope.cities + "]";
    $http.get(searchUrl).then(function(response){
      $scope.companies = response.data
      $scope.inputsDisabled = false;
    })
  }

  $scope.searchByCity = function(cityState){
    city = cityState.split(", ")[0];
    $scope.cities.push(city);
    $scope.clearCitySearch();
    doASearch();
  }

  $scope.unselectCity = function(city){
    $scope.cities.splice($scope.cities.indexOf(city), 1)
    doASearch();
  }

  $scope.searchByIndustry = function(industry){
    if (industry.checked){
      industryIds.push(industry.id)
    } else {
      industryIds.splice(industryIds.indexOf(industry.id), 1)
    }
    doASearch();
  }
  
  $scope.addCompany = function(company, size){
    company_id = company.id;
    open(size, company_id)
  }
  
  var open = function (size, company_id) {
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'targetForm.html',
      controller: 'ModalInstanceCtrl',
      size: size,
      resolve:{
        company_id: function(){
          return company_id;
        }
      }
    });
  };

}]);

app.controller("ModalInstanceCtrl", ["$scope", "$http", "$uibModalInstance", "$timeout", "company_id", function($scope, $http, $uibModalInstance, $timeout, company_id){
  
  var companyId = company_id
  
  $scope.cancel = function(){
    $uibModalInstance.dismiss('cancel');
  }
  
  $scope.updateTargetCompany = function(target_account){
    if (validTarget(target_account)) {
      target_account.company_id = company_id;
      target_account.quick_add = true;
      $http.post('/target', target_account).success(function(response){
        savedTarget(target_account);
        $scope.targetAdded = response;
        $scope.targetNotAdded = false;
        window.setTimeout(function() {$uibModalInstance.dismiss('cancel')}, 2000)
      }).error(function(response){
        $scope.targetNotAdded = response;
        $scope.targetAdded = false;
      })
    }
  }
  
  var validTarget = function(target_account) {
    if (typeof target_account === 'undefined') {
      $scope.targetNotAdded = ["Please fill out the form"]
      $scope.targetAdded = false;
    } else {
      return true;
    }
  }
  
  var savedTarget = function(target_account) {
    target_account.company_id = null;
    target_account.department_ids = null;
    target_account.contact_level_ids = null;
    $scope.targetAdded = null;
    $(".CaptionCont span").addClass("placeholder").text("Select options from below");
    $("li.selected").removeClass("selected");
  }
  
}])
