'use strict';

angular.module('app')
.controller("RegistrationController", ["$scope", "$http", "Data", function($scope, $http, Data){

  var industries = JSON.parse($("#industries").attr("data-url"));
  var focusOnIndustryIds = $("#focus_on_industry_ids").attr("data-url");
  var industryIds;
  var industryNames;
  var hidden_field = $("#user_target_industries_industry_id");
  var add = []
  $scope.industries = [];
  setIndustryIdsAndNames();
  addFocusOnIndustryIds();
  
  function addFocusOnIndustryIds(){
    var index;
    if (focusOnIndustryIds !== ''){
      focusOnIndustryIds = JSON.parse(focusOnIndustryIds)
      for(var i=0; i < focusOnIndustryIds.length; i++){
        index = industryIds.indexOf(parseInt(focusOnIndustryIds[i]))
        if (index > -1){
          $scope.industries.push(industries[index][1])
        }
      }
      add = focusOnIndustryIds;
      hidden_field.val(add)
    }
  }
  
  function setIndustryIdsAndNames(){
    industryIds = industries.map(function(industry){
      return industry[0];
    })
    
    industryNames = industries.map(function(industry){
      return industry[1];
    })
  }
  
  $scope.getCompanies = function(company){
    return $http.get('/api/v1/async_search', {
      params: {
        companies: company,
        sensor: false
      }
    }).then(function(response){
      if (response.data.length == 0){
        $scope.noResults = true;
      } else {
        $scope.noResults = false;
        return response.data;
      };
    });
  }
  
  $scope.getIndustries = function(term){
    var foundIndustries = industryNames.filter(function(industry){
      return industry.toLowerCase().search(term.toLowerCase()) !== -1;
    })
    return foundIndustries
  }
  
  $scope.storeIndustry = function(industry){
    for(var i = 0; i < industries.length; i++){
      if (industries[i][1] == industry){
        var index = i;
      }
    }
    if ($scope.industries.indexOf(industry) === -1){
      $scope.industries.push(industry)
    }
    add.push(industries[index][0])
    hidden_field.val(add);
    $scope.industry_selected = null;
  }
  
  $scope.removeIndustry = function(industry){
    var index = $scope.industries.indexOf(industry)
    $scope.industries.splice(index, 1);
    add.splice(index, 1)
    hidden_field.val(add)
  }

  $scope.clearForm = function(portfolio_account){
    portfolio_account.company = null;
    $scope.noResults = false;
  }

}])
