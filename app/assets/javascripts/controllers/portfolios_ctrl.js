'use strict';

var app = angular.module('app');

app.controller("PortfoliosController", ["$scope", "$http", "$window", "Data", function($scope, $http, $window, Data){

  var company_id;
  var new_company;

  Data.allIndustries().success(function(response){
    $scope.industries = response;
  })

  $scope.setCompanyInfo = function(company){
    $scope.logo = company.logo.url;
    company_id = company.id
  }

  $scope.updatePortfolio = function(portfolio_account, goToNext, goToProfile){
    if (portfolioIsDefined(portfolio_account)){
      portfolio_account.company_id = company_id;
      portfolio_account.new_company = new_company
      portfolio_account.next = goToNext;
      portfolio_account.profile = goToProfile;
      $http.post('/portfolio', portfolio_account).success(function(response){
        if (response.next){
          $window.location.href = '/submitted?submission=true';
        } else if (response.profile) {
          $window.location.href = '/profile';
        }
        else {
          savedPortfolio(portfolio_account)
          $scope.saved = response;
          $scope.hasErrors = false;
        }
      }).error(function(response){
        $scope.errors = response
        $scope.hasErrors = true;
        $scope.saved = false;
      })
    }
  }

  function portfolioIsDefined(portfolio_account){
    if (typeof portfolio_account !== 'undefined'){
      return true;
    } else {
      $scope.errors = ["Please fill out the form"];
      $scope.hasErrors = true;
      $scope.saved = false;
    }
  }

  $scope.addCompanyInfo = function(){
    $scope.addACompany = !$scope.addACompany;
    new_company = !new_company;
  }

  $scope.next = function(portfolio_account){
    if (typeof portfolio_account === 'undefined'){
      $window.location.href = '/submitted?submission=true';
    } else {
      $scope.updatePortfolio(portfolio_account, true, false);
    }
  }

  $scope.goToProfile = function(portfolio_account) {
    if (typeof portfolio_account === 'undefined') {
      $window.location.href = '/profile';
    } else {
      $scope.updatePortfolio(portfolio_account, false, true);
    }
  }

  $scope.hideAlert = function(){
    $scope.saved = null;
    $scope.hasErrors = null;
  }

  function formIsInvalid(portfolio_account){
    if (formIsBlank(portfolio_account)){
      $scope.errors = ["Please fill out the form"];
      return true;
    } else if (formIsPartial(portfolio_account)){
      $scope.errors = ["Please select a value for all fields"];
      return true;
    }
    return false
  }

  function formIsBlank(portfolio_account){
    return typeof portfolio_account === 'undefined'
  }

  function formIsPartial(portfolio_account){
    return typeof portfolio_account.department_ids === 'undefined' || typeof portfolio_account.contact_level_ids === 'undefined' || typeof portfolio_account.company === 'undefined'
  }

  function savedPortfolio(portfolioAccount){
      portfolioAccount.company = null;
      company_id = null;
      portfolioAccount.department_ids = null;
      portfolioAccount.contact_level_ids = null;
      portfolioAccount.industry_ids = null;
      portfolioAccount.city = null;
      portfolioAccount.state = null;
      $scope.addACompany = false;
      $scope.logo = null;
      $(".CaptionCont span").addClass("placeholder").text("Select options from below");
      $("li.selected").removeClass("selected");
  }

}])
