'use strict';

var app = angular.module('app');

app.controller("ApplicationController", ["$scope", "$http", "application", "Data", "$uibModal", function($scope, $http, application, Data, $uibModal){

  $scope.positions = ['Sales Professional', 'Consultant', 'Executive', 'Businesss Development'];

  function getUser(){
    if (location.pathname === '/profile'){
      $http.get('../api/v1/current_user').then(function(response){
        $scope.user = response.data
      })
    }
  }

  getUser();

  Data.allIndustries().success(function(response){
    $scope.industries = response;
  })

  Data.allDepartments().success(function(response){
    $scope.departments = response;
  })

  $scope.updateUser = function(user){
    application.updateApplication(user).success(function(response){
      disableEditors()
      $scope.user = response.user
      $scope.saved = true;
      $scope.hasErrors = false;
    }).error(function(response){
      $scope.errors = response
      $scope.hasErrors = true;
      $scope.saved = false;
    })
  }
  
  $scope.openUserForm = function(user, size) {
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'userForm.html',
      controller: 'ModalInstanceController',
      size: size,
      resolve: {
        user: function() {
          return user;
        }
      }
    });
  }
  
  function disableEditors() {
    $scope.editPosition = false
    $scope.editSells = false;
    $scope.editAbout = false;
  }
  
  $scope.enableEditor = function(editor){
    if (editor == 'position') {
      $scope.editPosition = !$scope.editPosition;
    } else if (editor == 'sells') {
      $scope.editSells = !$scope.editSells;
    } else if (editor == 'about') {
      $scope.editAbout = !$scope.editAbout;
    }
  }
  
  $scope.showPortfolio = true;

  $scope.showTab = function(tab){
    if (tab === 'portfolio'){
      $scope.showTargets = false;
      $scope.showPortfolio = true;
    }else {
      $scope.showPortfolio = false;
      $scope.showTargets = true;
    }
  }

  $scope.peer = true;
  $scope.showAnonymous = function(){
    $scope.anonymous = true;
    $scope.peer = false;
  }
  
  $scope.showPeer = function(){
    $scope.peer = true;
    $scope.anonymous = false
  }
  
  function showTargets(){
    var target = $("#target").attr("data-url")
    if (target){
      $scope.showTargets = true;
      $scope.showPortfolio = false;
    }
  }
  
  showTargets();

}]);

app.controller("AvatarController", ["$scope", "$http", "Data", function($scope, $http, Data) {
  
  $scope.showAvatars = function() {
    $scope.spinner = true;
    Data.getAvatars().success(function(response) {
      $scope.avatars = response;
      $scope.spinner = false;
    })
  }
  
  var previous_avatar = {};
  $scope.assignAvatar = function(user, avatar, url, id){
    avatar.selected = true;
    user.avatar = avatar.image.url;
    previous_avatar.selected = false;
    previous_avatar = avatar;
  };
  
}]);

app.controller("ModalInstanceController", ["$scope", "$http", "$uibModalInstance", "application", "user", "Data", function($scope, $http, $uibModalInstance, application, user, Data) {
  
  Data.getStates().success(function(response) {
    $scope.states = response;
  })
  $scope.user = user;
  
  $scope.closeModal = function() {
    $uibModalInstance.dismiss('cancel');
  };
  
  $scope.updateUserModal = function(user){
    application.updateApplication(user).success(function(response){
      $scope.user = response.user;
      $scope.success = response.message;
      $scope.error = false;
      window.setTimeout(function() {$uibModalInstance.dismiss('cancel')}, 2000)
    }).error(function(response){
      $scope.user = response.user;
      $scope.errors = response.message;
      $scope.success = false;
    })
  };
  
}])

app.controller("PeerMessageController", ["$scope", "$http", "$timeout", "$window", function($scope, $http, $timeout, $window){
  message = this;
  message.messageBody = "Hello! Here is some filler text for the message."
  message.requestBody = "Hello! Here is some filler text for the message."
  
  message.sendMessage = function(member_id, messageParams) {
    params = {
      subject: messageParams.messageSubject,
      body: messageParams.messageBody
    }
    $http.post('/send_message/' + member_id + '.json', params)
    .success(function(response) {
      message.messageSuccess = response;
      $timeout(function() {
        message.messageSuccess = false;
      }, 2000)
      message.subject = null
      message.messageBody = "Hello! Here is some filler text for the message.";
      $("#peer-message").toggle();
    }).error(function(response) {
      message.messageError = response;
      $timeout(function() {
        message.messageError = false;
      }, 2000)
    })
  }
  
  message.sendPeerRequest = function(member_id, requestParams) {
    params = {
      member_id: member_id,
      body: requestParams.requestBody
    }
    $http.post('/api/v1/connections', params)
    .success(function(response) {
      $timeout(function() {
        message.requestSuccess = false;
        $window.location.href = '/members/' + params["member_id"];
      }, 2000)
      message.requestSuccess = response;
      message.requestBody = "Hello! Here is some filler text for the message."
      $("#peer-request").toggle();
    }).error(function(response) {
      $timeout(function() {
        message.requestError = false;
      }, 2000)
      message.requestError = response;
    })
  }
}]);

app.config(["$httpProvider", function($httpProvider){
  $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');
}]);
