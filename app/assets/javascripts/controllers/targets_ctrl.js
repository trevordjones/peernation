'use strict';

angular.module('app')
.controller("TargetsController", ["$scope", "$http", "$window", "Data", function($scope, $http, $window, Data){
  target = this;
  var company_id;
  var new_company;
  
  target.addCompanyInfo = function(){
    target.addACompany = !target.addACompany;
    new_company = !new_company;
  }
  
  target.setCompanyInfo = function(company){
    target.logo = company.logo.url;
    company_id = company.id;
  }
  
  target.updateTarget = function(target_account, goToNext){
    if (targetIsDefined(target_account)) {
      target_account.company_id = company_id;
      target_account.new_company = new_company;
      target_account.next = goToNext;
      $http.post('/target', target_account).success(function(response){
        if (response.next){
          $window.location.href = '/profile'
        } else {
          target.saved = response;
          savedTarget(target_account);
          target.hasErrors = false;
        }
      }).error(function(response){
        target.hasErrors = true;
        target.saved = false;
        target.errors = response;
      })
    }
  }
  
  target.next = function(target_account){
    if (typeof target_account === 'undefined'){
      $window.location.href = '/profile'
    } else {
      target.updateTarget(target_account, true)
    }
  }
  
  target.hideAlert = function(){
    target.saved = null;
    target.hasErrors = null;
  }
  
  function targetIsDefined(target_account){
    if (typeof target_account !== 'undefined'){
      return true;
    } else {
      target.errors = ["Please fill out the form"]
      target.hasErrors = true;
      return false;
    }
  }
  
  function savedTarget(target_account){
    target_account.company = null;
    company_id = null;
    target_account.department_ids = null;
    target_account.contact_level_ids = null;
    target_account.industry_ids = null;
    target_account.city = null;
    target_account.state = null;
    target.addACompany = false;
    target.addACompany = false;
    $scope.noResults = false;
    $(".CaptionCont span").addClass("placeholder").text("Select options from below");
    $("li.selected").removeClass("selected");
  }
  
}])
