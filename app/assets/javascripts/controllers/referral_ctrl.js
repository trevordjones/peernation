'use strict';

angular.module('app')
.controller("ReferralController", ["$scope", "$http", "Data", function($scope, $http, Data){
  var refs = this;
  
  var listPortfolio = function(portfolio){
    return portfolio.map(function(company){
      return '<li><strong>' + company.name + '</strong></li>'
    })
  };
  
  var initialText = function(){
    return '<p>I would like you to join my Peer Nation network. Below are the portfolio companies '
    + 'that I have a relationship with and would be open to helping you with. Also, if you have any '
    + 'contacts within any of the companies in my target list I would appreciate your help.</p>'
    + '<p>Thanks,</p>'
    + '<p>' + refs.name + '</p>'
    + '<p>MY PORTFOLIO COMPANIES - Companies I may be able to help you with.</p>'
    + '<ul>' + listPortfolio(refs.portfolio).join(", ").replace(/,/g, '') + '</ul>'
    + 'My TARGET LIST - I would appreciate your help with these companies.'
    + listPortfolio(refs.target).join(", ").replace(/,/g, '')
  };
  
  Data.portfolio().success(function(response){
    refs.portfolio = response.portfolio;
    refs.target = response.target;
    refs.name = response.name
    refs.referrals = {
      text: initialText()
    }
  });
  
  refs.refer = function(referrals){
    $http.post('/api/v1/referrals', referrals).success(function(response){
      refs.class = 'success';
      refs.response = response;
      refs.referrals = {
        emails: null,
        text: initialText()
      }
    }).error(function(response){
      refs.class = 'danger';
      refs.response = response;
    })
  };
}])
