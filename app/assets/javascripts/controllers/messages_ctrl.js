'use strict';

angular.module('app').
  controller('MessagesController', ["$scope", "$http", "Data", function($scope, $http, Data){
    var msgs = this;
    
    msgs.getMessages = function(id){
      Data.getMessages(id).success(function(response){
        msgs.sender = response.new_sender
        msgs.receivers = response.receivers;
        msgs.conversation = response.conversation;
        msgs.messages = response.messages;
      })
    }
    
    msgs.showReply = function(){
      msgs.showReplyBox = !msgs.showReplyBox;
    }
    
    msgs.getConnections = function(recipient){
      return $http.get('/api/v1/recipients.json', {
        params: {
          recipient: recipient
        }
      }).then(function(response){
        return response.data.recipients
      })
    }
    
    msgs.addReceiver = function(addedReceiver){
      var selectedReceiver = msgs.receivers.filter(function(receiver){
        return addedReceiver.id === receiver.id
      })
      if (selectedReceiver.length < 1){
        msgs.receivers.push(addedReceiver)
      }
    }
    
    msgs.removeReceiver = function(removedReceiver){
      var index;
      msgs.receivers.map(function(receiver, ind){
        if (receiver.id == removedReceiver.id){
          index = ind;
        }
      })
      msgs.receivers.splice(index, 1)
    }
    
    var formIsValid = function(message){
      if (typeof message === 'undefined'){
        msgs.messageError = 'Please fill out the form';
        return false
      } else if (msgs.receivers.length < 1){
        msgs.messageError = 'You must select at least one recipient'
        return false
      } else {
        return true
      }
    }
    
    msgs.sendReplyMessage = function(message, sender){
      if (formIsValid(message)){
        message.ids = msgs.receivers.map(function(receiver){
          return receiver.id
        })
        message.conversation_id = msgs.conversation.id;
        $http.post('/messages', message).success(function(response){
          msgs.messageSuccess = response;
          msgs.messageError = false;
          message.sender = msgs.sender;
          msgs.messages.unshift(message)
          message = null;
          msgs.showReplyBox = !msgs.showReplyBox;
        }).error(function(response){
          msgs.messageError = response;
          msgs.messageSuccess = false;
        })
      }
    }
    
    msgs.discardMessage = function(message){
      message = null;
      msgs.showReplyBox = !msgs.showReplyBox;
    }
    
    msgs.displayMessage = function(message){
      message.full_message = !message.full_message;
    }
    
  }]);
