'use strict';

angular.module('app')
.controller("MemberRequestsController", ["$scope", "$http", function($scope, $http){

  $scope.memberRequest = function(request){
    $http.post('/member_requests', request).success(function(response){
      $scope.response = response;
      $scope.submitted = true;
      $scope.notSubmitted = false;
      $scope.request = {};
    })
    .error(function(response){
      $scope.responses = response
      $scope.notSubmitted = true;
      $scope.submitted = false;
    })
  }

  $scope.roles = ['Sales Professional', 'Consultant', 'Executive', 'Businesss Development']

}])
