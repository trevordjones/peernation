'use strict;'

var app = angular.module('app');

app.controller("UserIndustriesController", ["Data", "$uibModal", function(Data, $uibModal) {
  industry = this;
  
  Data.userIndustries().success(function(response){
    industry.user_industries = response;
  })
  
  industry.addIndustry = function() {
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'industriesFocusedOn.html',
      controller: 'IndustriesModal as modal',
      size: ''
    });
  }
}]);

app.controller('IndustriesModal', ["$http", "Data", "$window", "$uibModalInstance", function($http, Data, $window, $uibModalInstance) {
  modal = this;
  var industry_ids = [];
  
  Data.allIndustries().success(function(response) {
    modal.industries = response;
  })
  
  Data.userIndustries().success(function(response) {
    industry_ids = response.map(function(industry) {
      return industry.id
    })
  })
  
  modal.assigned = function(id) {
    return industry_ids.indexOf(id) > -1;
  }
  
  modal.handleIndustry = function(id) {
    var index = industry_ids.indexOf(id);
    if (index > -1) {
      industry_ids.splice(index, 1);
    } else {
      industry_ids.push(id)
    }
  }
  
  modal.updateIndustries = function() {
    $http.patch('/api/v1/user_industries', {industry_ids: industry_ids}).then(function(response) {
      modal.success = response.data.message;
      window.setTimeout(function() {$window.location.reload()}, 2000)
    })
  }
  
  modal.close = function() {
    $uibModalInstance.dismiss('cancel');
  }
}]);

app.controller('UserDepartmentsController', ["Data", "$uibModal", function(Data, $uibModal) {
  department = this;
  
  Data.userDepartments().success(function(response) {
    department.user_departments = response;
  });
  
  department.addDepartment = function() {
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'departmentsSellTo.html',
      controller: 'departmentsModal as modal',
      size: ''
    });
  }
  
}]);

app.controller('departmentsModal', ["$http", "$uibModalInstance", "$window", "Data", function($http, $uibModalInstance, $window, Data) {
  modal = this;
  var department_ids = [];
  
  Data.allDepartments().success(function(response) {
    modal.departments = response;
  });
  
  Data.userDepartments().success(function(response) {
    department_ids = response.map(function(department) {
      return department.id
    })
  });
  
  modal.updateDepartments = function() {
    $http.patch('/api/v1/user_departments', {department_ids: department_ids}).then(function(response) {
      modal.success = response.data.message;
      $window.setTimeout(function() {$window.location.reload()}, 2000)
    })
  }
  
  modal.assigned = function(id) {
    return department_ids.indexOf(id) > -1;
  }
  
  modal.handleDepartment = function(id) {
    var index = department_ids.indexOf(id);
    if (index > -1) {
      department_ids.splice(index, 1);
    } else {
      department_ids.push(id)
    }
  }
  
  modal.close = function() {
    $uibModalInstance.dismiss('cancel');
  }
}]);