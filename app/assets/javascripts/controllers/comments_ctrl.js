'use strict';

var app = angular.module('app');

app.controller("CommentsController", ["$scope", "$http", "Data", "$window", "$location", "$uibModal", function($scope, $http, Data, $window, $location, $uibModal){

  var companyId = $("#company-id").attr("data-url");
  if ($("#show-posts").attr("data-url") === "true"){
    $scope.showPosts = true;
  } else {
    $scope.showMembers = true;
  }

  var getMembers = function(id){
    $http.get('/api/v1/company_members/' + id + '.json').then(function(response){
      $scope.members = response.data.members;
    });
  };
  getMembers(companyId);

  $scope.comment = {};
  $scope.commentTypeOptions = [
    {name: "Contact", value: "Contact"},
    {name: "Regular", value: "Regular"}
  ];

  $scope.newComment = false;

  function getComments(){
    $http.get('/comments').then(function(response){
      $scope.comment = response.data;
    });
  }

  Data.allDepartments().success(function(response){
    $scope.departments = response;
  })

  Data.allContactLevels().success(function(response){
    $scope.contactLevels = response;
  })

  $scope.getLocation = function(city){
    return $http.get('/api/v1/member_city_search', {
      params: {
        city: city
      }
    }).then(function(response){
      $scope.noResults = false;
      if (response.data.length === 0) {
        $scope.noResults = true;
      }
      return response.data;
    })
  }

  $scope.clearCitySearch = function(){
    $scope.locationSelected = null;
    $scope.noResults = false;
  }

  $scope.formComment = {visibility: 'All'}

  $scope.postComment = function(comment){
    if (comment.comment) {
      comment.comment_type = "Comment"
    } else if (comment.question) {
      comment.comment_type = "Question"
    } else if (comment.contact) {
      comment.comment_type = "Contact"
    } else if (typeof comment.comment_type === 'undefined') {
      comment.comment_type = "Comment"
    }
    $http.post('/comments', comment).success(function(response){
      var date = new Date(response.created_at);
      date = date.getTime();
      response.created_at = date;
      $scope.comment = response;
      $scope.newComment = true;
      $scope.targetNodAdded = false;
      clearCommentForm();
    }).error(function(response){
      $scope.targetNotAdded = response;
    })
  };

  $scope.showContactForm = function(commentType){
    if (commentType == "Contact") {
      $scope.contactForm = true;
    } else {
      $scope.contactForm = false;
    }
  };

  function clearCommentForm(){
    $scope.formComment = {
      content: null,
      comment_type: $scope.commentTypeOptions[0].value
    };

    $scope.replyComment = {
      content: null,
      comment_type: $scope.commentTypeOptions[0].value
    };
  }

  $scope.replyForm = function(comment_id){
    event.preventDefault();
    var replyForm = $("#form-" + comment_id);
    replyForm.toggleClass("hidden");
  };

  $scope.showReplyComments = function(comment_id, comment){
    event.preventDefault();
    var comment_div = $("#" + comment_id);
    comment_div.toggleClass("hidden");
    if ($scope.comment[comment_id] === undefined){
      $scope.comment[comment_id] = comment_id;
    } else {
      $scope.comment[comment_id] = undefined;
    }
  };

  $scope.showTab = function(show){
    if (show === 'posts'){
      $scope.showPosts = true;
      $scope.showMembers = false;
    } else {
      $scope.showMembers = true;
      $scope.showPosts = false;
    }
  };

  var industryIds = [], departmentIds = [], contacts = [], mainUrl = '/api/v1/company_members_search/' + companyId + '.json', searchUrl
  $scope.cities = [];

  function doASearch(){
    $scope.inputsDisabled = true;
    searchUrl = mainUrl + "?department_ids=[" + departmentIds + "]&industries=[" + industryIds + "]&cities=[" + $scope.cities + "]&contacts=[" + contacts + "]";
    $http.get(searchUrl).then(function(response){
      $scope.members = response.data.members;
      $scope.inputsDisabled = false;
    });
  };

  $scope.searchByCity = function(cityState){
    var city = cityState.split(",")[0];
    $scope.cities.push(city);
    doASearch();
  };

  $scope.unselectCity = function(city){
    $scope.cities.splice($scope.cities.indexOf(city), 1);
    doASearch();
  };

  $scope.searchByIndustry = function(industry){
    if (industry.checked){
      industryIds.push(industry.id);
    } else {
      industryIds.splice(industryIds.indexOf(industry.id), 1);
    }
    doASearch();
  };

  $scope.searchByDepartment = function(department){
    if (department.checked){
      departmentIds.push(department.id);
    } else {
      departmentIds.splice(departmentIds.indexOf(department.id), 1);
    }
    doASearch();
  };

  $scope.searchByContactLevel = function(contact){
    if (contact.checked){
      contacts.push(contact.id);
    } else {
      contacts.splice(contacts.indexOf(contact.id), 1);
    }
    doASearch();
  };

  $scope.searchByPeers = function(checked){
    if (checked){
      $http.get('/api/v1/company_peers/' + companyId + '.json').then(function(response){
        $scope.members = response.data.members;
      })
    } else {
      doASearch();
    }
  }
  var currentLocation;

  $scope.searchComments = function(commentType, questionType, contactType, id, keyword){
    commentType = (typeof commentType === 'undefined') ? false : commentType
    questionType = (typeof questionType === 'undefined') ? false : questionType
    contactType = (typeof contactType === 'undefined') ? false : contactType
    keyword = (typeof keyword === 'undefined') ? "" : keyword
    document.location.search = "?comment=" + commentType.toString() + "&question=" + questionType.toString() + "&contact=" + contactType.toString() + "&keyword=" + keyword + "&post=true";
  }

  var showPortfolio;
  $scope.showMorePortfolio = function(index){
    return index === 0 || showPortfolio;
  }

  $scope.showPortfolio = function(){
    showPortfolio = !showPortfolio;
  }
  
  $scope.updateTargetCompany = function(){
    var targetParams = {
      id: companyId
    }
    $http.post('/target', targetParams).success(function(response){
      $scope.targetAdded = response;
      $scope.targetNotAdded = false;
    }).error(function(response){
      $scope.targetNotAdded = response;
      $scope.targetAdded = false;
    })
  }
  
  $scope.open = function (size) {
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'targetForm.html',
      controller: 'ModalInstanceCtrl',
      size: size,
      resolve:{
        company_id: function(){
          return companyId;
        }
      }
    });
  };

}]);
