'use strict';

angular.module('app').
  controller("ConversationsController", ["$scope", "$http", "Data", "$window", function($scope, $http, Data, $window){
    var messageIds = [];
    var recipientIds = [];
    var replyMessage;
    $scope.recipients = [];
    $scope.totalReceivedPages = $("#received-pages").attr('data-url');
    $scope.totalSentPages = $("#sent-pages").attr('data-url');
    $scope.totalArchivedPages = $('#archived-pages').attr('data-url');
    $scope.totalTrashedPages = $('#trashed-pages').attr('data-url');
    senderAsRecipient()

    var conversationValid = function(conversation){
      if (typeof conversation === 'undefined') {
        $scope.errorMessage = "Please fill out the form";
        return false;
      } else if (typeof conversation.subject === 'undefined' || conversation.subject === "") {
        $scope.errorMessage = "Subject can't be blank";
        return false;
      } else if (typeof conversation.body === 'undefined' || conversation.body === "") {
        $scope.errorMessage = "Body can't be blank";
        return false;
      } else if (recipientIds.length === 0){
        $scope.errorMessage = "Please select a recipient";
        return false;
      } else {
        return true;
      }
    }
    
    $scope.startConversation = function(conversation){
      if (conversationValid(conversation)){
        conversation.ids = recipientIds;
        $http.post('/inbox', conversation).success(function(response, status, headers, config){
          $scope.errorMessage = null;
          $scope.successMessage = response;
          $scope.recipients = null;
          $scope.conversation = {};
        }).
        error(function(response){
          $scope.errorMessage = response;
          $scope.successMessage = null;
        })
      }
    }
    
    $scope.removeRecipient = function(removedRecipient){
      var index = recipientIds.indexOf(removedRecipient.id);
      recipientIds.splice(index, 1);
      $scope.recipients.map(function(recipient, ind){
        if (recipient.email == removedRecipient.email){
          index = ind;
        }
      })
      $scope.recipients.splice(index, 1);
    }

    $scope.sendMessage = function(message){
      $http.post('/messages', message).success(function(response, status){
        $scope.response = response;
        $scope.message = {};
      })
      .error(function(responses, status){
        $scope.responses = responses
      })
    }

    $scope.showReplyForm = function(){
      $scope.showForm = true;
    }

    $scope.page = 1;
    function getMessages(page){
      if ($scope.totalSentPages){
        $scope.getSentMessages(page);
      } else if ($scope.totalReceivedPages){
        $scope.getReceivedMessages(page);
      } else if ($scope.totalArchivedPages){
        $scope.getArchivedMessages(page);
      } else if ($scope.totalTrashedPages){
        $scope.getTrashedMessages(page);
      }
    }

    function senderAsRecipient(){
      var senderId = $("#send-to").val();
      if (senderId){
        $http.get('/api/v1/recipients/' + senderId + '.json').success(function(response){
          $scope.recipients.push(response);
          recipientIds.push(response.id);
        })
      }
    }

    $scope.postReply = function(reply){
      reply.ids = recipientIds
      $http.post('/inbox/reply/' + reply.message_id, reply).success(function(response){
        $window.location.href = "/inbox"
      })
    }

    $scope.getReceivedConversations = function(page){
      Data.receivedConversations(page).success(function(response){
        $scope.receivedMessages = response.received;
        $scope.totalReceivedPages = response.total_pages;
      });
    }

    $scope.getSentConversations = function(page){
      Data.sentConversations(page).success(function(response){
        $scope.sentMessages = response.sent;
        $scope.totalSentPages = response.total_pages;
      });
    }

    $scope.getArchivedConversations = function(page){
      Data.archivedConversations(page).success(function(response){
        $scope.archivedConversations = response.archived;
        $scope.totalArchivedPages = response.total_pages;
      })
    }

    $scope.getTrashedConversations = function(page){
      Data.trashedConversations(page).success(function(response){
        $scope.trashedConversations = response.trashed;
        $scope.totalTrashedPages = response.total_pages;
      })
    }

    $scope.addIds = function(checked, id){
      if (checked){
        messageIds.push(id);
      } else {
        var index = messageIds.indexOf(id, messageIds);
        messageIds.splice(index, 1);
      }
    }

    $scope.isActive = function(option){
      if ($scope.active == option) {
        return true;
      } else {
        return false;
      };
    }

    // $scope.activate = function(option){
    //   $scope.active = option;
    //   getMessages($scope.page)
    // }

    $scope.page = 1;

    $scope.nextPage = function(totalPages){
      if ($scope.page == totalPages) {
        $scope.page = 1;
      } else {
        $scope.page ++;
      }
      getMessages($scope.page);
    }

    $scope.prevPage = function(totalPages){
      if ($scope.page == 1) {
        $scope.page = totalPages
      } else {
        $scope.page --;
      }
      getMessages($scope.page)
    }

    $scope.archiveMessages = function(){
      $http.patch('/api/v1/archive_conversations?conversation_ids=[' + messageIds + ']').then(function(response){
        $window.location.reload();
      })
    }

    $scope.archiveMessage = function(id){
      messageIds.push(id)
      $scope.archiveMessages();
    }

    $scope.trashMessages = function(){
      $http.patch('/api/v1/trash_conversations?conversation_ids=[' + messageIds + ']').success(function(response){
        $window.location.reload();
      }).error(function(response){
        $scope.messageError = response;
      })
    }

    $scope.trashMessage = function(id){
      messageIds.push(id)
      $scope.trashMessages();
    }

    $scope.getConnections = function(recipient){
      return $http.get('/api/v1/recipients.json', {
        params: {
          recipient: recipient
        }
      }).then(function(response){
        return response.data.recipients
      })
    }

    $scope.addRecipient = function(recipient){
      if ($scope.recipients.length === 0){
        $scope.recipients.push(recipient)
        recipientIds.push(recipient.id);
      } else {
        for (var i = 0; i < $scope.recipients.length; i++){
          if ($scope.recipients[i].email !== recipient.email){
            $scope.recipients.push(recipient);
            recipientIds.push(recipient.id);
          }
        }
      }
    }

  }])
  .filter('cut', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                    value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
    });
