'use strict';

angular.module('app')
.controller("NotificationsController", ["$scope", "$http", "Data", function($scope, $http, Data){
  var notes = this;
  notes.noteLimit = 10;
  
  Data.userNotifications().success(function(response){
    notes.notifications = response.notifications;
  })
  
  notes.deleteNotification = function(id){
    $http.delete('/api/v1/notifications/' + id.toString()).success(function(response){
      var index = notes.notifications.map(function(note) {return note.id; }).indexOf(response.id);
      notes.notifications.splice(index, 1);
      notes.noteDeleted = "Note was deleted!";
      notes.notDeleted = false
    }).error(function(response){
      notes.notDeleted = response;
      notes.noteDeleted = false;
    })
  }
  
  notes.olderNotifications = function(){
    notes.noteLimit += 10;
    console.log(notes.noteLimit)
  }
}])
