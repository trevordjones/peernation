'use strict';

angular.module('app')
.controller("MainSearchController", ["$scope", "$http", "Data", function($scope, $http, Data){

  $scope.getCompanies = function(company){
    return $http.get('/api/v1/async_search', {
      params: {
        companies: company,
        sensor: false
      }
    }).then(function(response){
      if (response.data.length == 0){
        $scope.noResults = true;
      } else {
        $scope.noResults = false;
        return response.data;
      };
    });
  }

  $scope.clearForm = function(portfolio_account){
    portfolio_account.company = null;
    $scope.noResults = false;
  }

  $scope.closeAlert = function(){
    $scope.class = null;
    $scope.response = null;
  }

}])
