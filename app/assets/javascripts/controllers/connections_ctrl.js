'use strict';

var app = angular.module('app')

app.controller("ConnectionsController", ["$scope", "$http", "connect", "Data", function($scope, $http, connect, Data){

  var industryIds = [], departmentIds = [], contacts = [], searchUrl = '/api/v1/member_search';
  $scope.cities = [];

  $scope.makeRequest = function(id){
    connect.postConnection(id).success(function(response, status, headers, config){
      $scope.response = response;
      $scope.madeRequest = true;
      $scope.unsuccessfulRequest = false;
    }).error(function(response, status, headers, config){
      $scope.response = response;
      $scope.unsuccessfulRequest = true;
      $scope.madeRequest = false;
    });
  };

  $scope.updateRequest = function(user, answer){
    var params = {
      user_id: user,
      answer: answer
    }
    $http.put('/connections/' + user, params).success(function(response, status, headers, config){
      $scope.notice = response;
    }).error(function(response, status, headers, config){
      $scope.notice = "Something went wrong. Please try that again"
    })
  }

  $scope.tabs = {
    myPeers: true,
    peerRequests: false,
    myPeerRequests: false,
    matches: false
  }

  var lastActiveTab = 'myPeers';

  $scope.activeTab = function(tab){
    if ($scope.tabs[tab] !== true){
      $scope.tabs[tab] = !$scope.tabs[tab]
      $scope.tabs[lastActiveTab] = !$scope.tabs[lastActiveTab]
    }
    lastActiveTab = tab;
  }

  activeTab($("#show-connections").attr("data-url"))

  function activeTab(tab){
    if (tab === 'approvals'){
      $scope.showWaitingApproval = true;
    } else {
      $scope.showAllConnections = true;
    }
  }

  function memberLocation(members){
    var allCities = members.map(function(member){ return member.city + ", " + member.state });
    var unique = allCities.filter(uniqueMemberCities);
    $scope.locations = unique;
  }

  function uniqueMemberCities(value, index, self){
    return self.indexOf(value) === index;
  }


  var getMemberData = function(){
    if (location.pathname === "/members"){
      Data.allMembers().success(function(response){
        $scope.members = response.members;
        memberLocation($scope.members);
      })
      Data.portfolioIndustries().success(function(response){
        $scope.industries = response.industries;
      })

      Data.portfolioDepartments().success(function(response){
        $scope.departments = response.departments;
      })
    }
  }
  getMemberData();


  var showPortfolio;
  $scope.showMorePortfolio = function(index){
    return index === 0 || showPortfolio;
  }

  $scope.showPortfolio = function(){
    showPortfolio = !showPortfolio;
  }

  $scope.getLocation = function(city){
    return $http.get('/api/v1/member_city_search', {
      params: {
        city: city
      }
    }).then(function(response){
      $scope.noResults = false;
      if (response.data.length === 0) {
        $scope.noResults = true;
      }
      return response.data;
    })
  }

  Data.allContactLevels().success(function(response){
    $scope.contactLevels = response;
  })

  function doASearch(){
    $scope.inputsDisabled = true;
    searchUrl = '/api/v1/member_search.json' + "?department_ids=[" + departmentIds + "]&industries=[" + industryIds + "]&cities=[" + $scope.cities + "]&contacts=[" + contacts + "]";
    $http.get(searchUrl).then(function(response){
      $scope.members = response.data.members;
      $scope.inputsDisabled = false;
    })
  }

  $scope.clearCitySearch = function(){
    $scope.locationSelected = null;
    $scope.noResults = false;
  }

  $scope.searchByCity = function(cityState){
    var city = cityState.split(",")[0]
    $scope.cities.push(city);
    $scope.clearCitySearch();
    doASearch();
  }

  $scope.unselectCity = function(city){
    $scope.cities.splice($scope.cities.indexOf(city), 1)
    doASearch();
  }

  $scope.searchByIndustry = function(industry){
    if (industry.checked){
      industryIds.push(industry.id)
    } else {
      industryIds.splice(industryIds.indexOf(industry.id), 1)
    }
    doASearch();
  }

  $scope.searchByDepartment = function(department){
    if (department.checked){
      departmentIds.push(department.id)
    } else {
      departmentIds.splice(departmentIds.indexOf(department.id), 1)
    }
    doASearch();
  }

  $scope.searchByPeers = function(checked){
    if (checked){
      $http.get('/api/v1/peers.json').then(function(response){
        $scope.members = response.data.members;
      })
    } else {
      doASearch();
    }
  }

  $scope.searchByContactLevel = function(contact){
    if (contact.checked){
      contacts.push(contact.id)
    } else {
      contacts.splice(contacts.indexOf(contact.id), 1)
    }
    doASearch();
  }

}])
.filter('cut', function () {
      return function (value, wordwise, max, tail) {
          if (!value) return '';

          max = parseInt(max, 10);
          if (!max) return value;
          if (value.length <= max) return value;

          value = value.substr(0, max);
          if (wordwise) {
              var lastspace = value.lastIndexOf(' ');
              if (lastspace != -1) {
                  value = value.substr(0, lastspace);
              }
          }

          return value + (tail || ' …');
      };
  });
