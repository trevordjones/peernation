CKEDITOR.editorConfig = function (config) {

  config.toolbar_mini = [
    ["Bold",  "Italic",  "Underline",  "Strike",  "-",  "Subscript",  "Superscript"],
  ];
  // config.toolbar = "simple";
  config.toolbar = [
    {name: 'basicstyles', items: ['Bold', 'Italic', 'Strike']},
    {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']}
  ]

}
