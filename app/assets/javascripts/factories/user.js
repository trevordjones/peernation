var app = angular.module('app');

app.factory("application", ["$http", function($http){
  var application = {}
  var urlBase = '/profile'

  application.updateApplication = function(user){
    return $http.patch(urlBase, user)
  }
  return application
}])
