'use strict';

angular.module('app')
.factory("search", function SearchFactory($http){

  var members; //this is the variable that holds searched for companies
  var allMembers, city, industrySearched;
  var checkedIndustries = [];
  var membersByCity = [];
  var membersByIndustry = [];

  $http.get('/api/v1/users.json').then(function(response){
    allMembers = response.data.members;
  });

  return {
    getAllMembers: function(){
      return allMembers;
    },
    byCity: function(city){
      return function(company){
        return company.city == city;
      }
    },
    filteredByCity: function(city){
      return city;
    },
    filteredByIndustry: function(industry){
      if (checkedIndustries.length == 0){
        industrySearched = false;
      } else {
        industrySearched = industry
      }
      return industrySearched;
    },
    typeahead: function(cityState){
      city = cityState.split(", ")[0]
      //if an industry has been checked
      if (industrySearched){
        members = industrySearched.members
        membersByCity = members.filter(this.byCity(city))
      } else {
        allMembers = this.getAllMembers();
        membersByCity = allMembers.filter(this.byCity(city));
      }
      this.filteredByCity(city);
      return membersByCity;
    },
    industriesChecked: function(industry){
      if (checkedIndustries.length > 0){
        var removedIndustry = checkedIndustries.pop();
        removedIndustry.checked = false;
      }
      checkedIndustries.push(industry)
      return checkedIndustries;
    },
    industriesUnchecked: function(industry){
      var index = $.inArray(industry, checkedIndustries)
      checkedIndustries.splice(index, 1)
      return checkedIndustries
    },
    industryFilter: function(industry){
      if (city){
        if (checkedIndustries.length == 0){
          membersByIndustry = this.getAllMembers().filter(this.byCity(city));
        } else {
          membersByIndustry = industry.members.filter(this.byCity(city))
        }
      } else {
        membersByIndustry = industry.members
      }
      this.filteredByIndustry(industry);

      return membersByIndustry
    }
  };
})
