'use strict';

var app = angular.module('app');

app.factory('request', ['$http', function($http){
  var request = {}
  var urlBase = '/requests'

  request.postRequest = function(requestUser, answer){
    var requestedParams = {
      requester: requestUser,
      answer: answer
    }
    return $http.post(urlBase, requestedParams)
  }
  return request
}])
