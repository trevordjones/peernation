'use strict';

angular.module('app')
.factory('Data', ['$http', function DataFactory($http){
  return {
    allIndustries: function(){
      return $http({method: 'GET', url: '/api/v1/industries.json'});
    },
    allDepartments: function(){
      return $http({method: 'GET', url: '/api/v1/departments.json'})
    },
    allCompanies: function(params){
      return $http({method: 'GET', url: '/api/v1/companies.json&' + params})
    },
    portfolioIndustries: function(){
      return $http({method: 'GET', url: '/api/v1/portfolio_industries.json'})
    },
    portfolioDepartments: function(){
      return $http({method: 'GET', url: '/api/v1/portfolio_departments.json'})
    },
    allMembers: function(){
      return $http({method: 'GET', url: '/api/v1/users.json'})
    },
    allContactLevels: function(){
      return $http({method: 'GET', url: '/api/v1/contact_levels.json'})
    },
    allMessages: function(page){
      return $http({method: 'GET', url: '/api/v1/conversations.json?page=' + page})
    },
    sentConversations: function(page){
      return $http({method: 'GET', url: '/api/v1/sent_conversations.json?page=' + page})
    },
    receivedConversations: function(page){
      return $http({method: 'GET', url: '/api/v1/received_conversations.json?page=' + page})
    },
    archivedConversations: function(page){
      return $http({method: 'GET', url: '/api/v1/archived_conversations.json?page=' + page})
    },
    trashedConversations: function(page){
      return $http({method: 'GET', url: '/api/v1/trashed_conversations.json?page=' + page})
    },
    getMessages: function(conversation_id){
      return $http({method: 'GET', url: '/api/v1/messages/' + conversation_id + '.json'})
    },
    allConnections: function(){
      return $http({method: 'GET', url: 'api/v1/connections.json'})
    },
    userNotifications: function(){
      return $http({method: 'GET', url: 'api/v1/notifications.json'})
    },
    portfolio: function(){
      return $http({method: 'GET', url: 'api/v1/referrals.json'})
    },
    getAvatars: function() {
      return $http({method: 'GET', url: 'api/v1/avatars.json'})
    },
    getStates: function() {
      return $http({method: 'GET', url: 'api/v1/states.json'})
    },
    userIndustries: function() {
      return $http({method: 'GET', url: 'api/v1/user_industries.json'})
    },
    userDepartments: function() {
      return $http({method: 'GET', url: 'api/v1/user_departments.json'})
    }
  }
}]);
