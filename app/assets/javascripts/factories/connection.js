'use strict';

var app = angular.module('app')

app.factory('connect', ['$http', function($http){
  var connect = {};
  var urlBase = '/connections';

  connect.postConnection = function(id){
    var user = {
      id: id
    }

    return $http.post(urlBase, user)
  }

  return connect
}])
