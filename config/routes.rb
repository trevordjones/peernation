Rails.application.routes.draw do
  devise_for :admins, controllers: {sessions: 'admin/sessions', registrations: 'admin/registrations'}
  devise_scope :admin do
    get 'delta', to: 'admin/sessions#delta'
    get 'admins/sign_in', to: 'admin/sessions#delta'
    delete 'logout', to: 'devise/sessions#destroy'
  end
  devise_for :users, :controllers => {:omniauth_callbacks => "users/omniauth_callbacks", :registrations => "users"}
  devise_scope :user do
    get 'login', to: 'devise/sessions#new'
    delete 'logout', to: 'devise/sessions#destroy'
  end

  root 'welcome#index'
  get '/ref/:referrer' => 'welcome#index'

  # get '/portfolio/new' => 'portfolio_accounts#new'
  # post '/portfolio' => 'portfolio_accounts#create'
  resources :portfolio, controller: 'portfolio_accounts'
  get '/target/new' => 'target_accounts#new'
  post '/target' => 'target_accounts#create'

  resources :companies
  get '/profile' => 'users#profile'
  put '/profile' => 'users#update'
  patch '/profile' => 'users#update'
  get '/profile/new' => 'users#new_profile'
  post '/profile' => 'users#create_profile'
  patch '/submit' => 'users#submit_application'
  get '/submitted' => 'users#submitted'
  get '/edit/password/:id' => 'users#edit_password', as: :edit_password
  patch '/edit/password/:id' => 'users#update_password', as: :update_password

  resources :members, only: [:index, :show, :update]

  resources :connections

  resources :requests, only: [:index, :create]
  resources :comments
  resources :member_requests, only: [:index, :create]
  resources :app_images, only: [:new, :create]

  namespace :admin do
    resources :applications, only: [:index, :show, :update]
    resources :members, only: [:index, :show, :new, :create]
    resources :member_requests, only: [:index, :destroy]
    resources :industries, only: [:new, :create]
    resources :departments, only: [:new, :create]
    get '/new_requests' => 'members#new_requests'
    get '/unapproved' => 'members#unapproved'
    get '/rejected' => 'members#rejected'
    get '/current' => 'members#current'
    get '/new_companies' => 'companies#index'
    put '/members/suspend_member/:id' => 'members#suspend_member', as: :suspend_member
    patch '/members/suspend_member/:id' => 'members#suspend_member'
    put '/members/approve_member/:id' => 'members#approve_member', as: :approve_member
    patch '/members/approve_member/:id' => 'members#approve_member'
    put '/members/disapprove_member/:id' => 'members#disapprove_member', as: :disapprove_member
    put '/members/disapprove_member/:id' => 'members#disapprove_member'
  end

  resources :inbox, controller: 'conversations', except: [:edit, :show, :update]
  get '/inbox/sent_conversations' => 'conversations#sent'
  get '/inbox/archived_conversations' => 'conversations#archived'
  get '/inbox/trashed_conversations' => 'conversations#trashed'
  match '/inbox/archive_conversations' => 'conversations#archive', via: [:put, :patch]
  match '/inbox/trash_conversations' => 'conversations#trash', via: [:put, :patch]
  get '/inbox/message/:id' => 'conversations#show', as: :inbox_message
  get '/inbox/reply/:id' => 'conversations#reply', as: :inbox_reply
  post '/inbox/reply/:id' => 'messages#create'
  post '/send_message/:id' => 'conversations#send_message'
  resources :messages, only: [:create]
  resources :notifications, only: [:index, :destroy]

  namespace :api do
    namespace :v1 do
      resources :companies, only: [:index]
      get '/company_search' => 'companies#search'
      get '/async_search' => 'companies#async_search'
      get '/company_city_search' => 'companies#city_search'
      get '/member_search' => 'members#search'
      get 'member_city_search' => 'members#city_search'
      get '/company_members/:id' => 'members#company_members'
      get '/company_members_search/:id' => 'members#company_members_search'
      get '/peers' => 'members#peers_search'
      get '/company_peers/:id' => 'members#company_peers_search'
      resources :comments, only: [:index]
      resources :departments, only: [:index]
      resources :industries, only: [:index, :show]
      resources :users, only: [:index, :update]
      get '/user_industries' => 'users#industries'
      patch '/user_industries' => 'users#update_industries'
      get '/user_departments' => 'users#departments'
      patch '/user_departments' => 'users#update_departments'
      get '/current_user' => 'users#user'
      resources :portfolio_industries, only: [:index]
      resources :portfolio_departments, only: [:index]
      resources :conversations, only: [:index]
      get '/sent_conversations' => 'conversations#sent'
      get '/received_conversations' => 'conversations#received'
      get '/archived_conversations' => 'conversations#archived'
      get '/trashed_conversations' => 'conversations#trashed'
      match '/archive_conversations' => 'conversations#archive', via: [:put, :patch]
      match '/trash_conversations' => 'conversations#trash', via: [:put, :patch]
      resources :contact_levels, only: [:index]
      resources :connections, only: [:index, :create]
      resources :messages, only: [:show]
      get '/recipients' => 'connections#recipients'
      get '/recipients/:id' => 'connections#recipient'
      resources :notifications
      resources :referrals, only: [:create, :index]
      get '/avatars' => 'users#avatars'
      get '/states' => 'users#states'
    end
  end
end
